#include "protodisplacement.h"

#include "protofilament.h"

MonoCoords::MonoCoords(const Monomer &mono) :
    number(mono.mono_number), r(mono.radii), z(mono.center.z)
{}

MonoCoords::MonoCoords(int numb, double r, double z) :
    number(numb), r(r), z(z)
{}

ProtoCoords::ProtoCoords(const Protofilament &proto, int64_t iteration) :
    iteration(iteration)
{
    for (const auto &mono : proto.monomers)
        monomers.push_back({mono});
}

ProtoDisplacement::ProtoDisplacement(const std::string &path, int64_t drop_count) :
    drop_count(drop_count)
{
    out = fopen(path.c_str(), "w");
    assert(out);
}

void ProtoDisplacement::saveProto(const Protofilament &proto, int64_t iteration)
{
    snapshots.push_back({proto, iteration});
    if (int64_t(snapshots.size()) < drop_count)
        return;

    assert(out);

    for (const auto &proto : snapshots) {
        fprintf(out, "%ld    ", proto.iteration);
        for (const auto &mono : proto.monomers)
            fprintf(out, " %f %f |", mono.r, mono.z);
        fprintf(out, "\n");
    }

    snapshots.clear();
}
