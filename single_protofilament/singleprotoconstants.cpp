#include "singleprotoconstants.h"

long write_transfer_rate = 1e7;
long write_transfer_offset = 1e7;

double left_zone = 0.31; //(nm) radial distance for lateral points distance 0.15
double right_zone = 0.73; //(nm) radial distance for lateral points distance 0.35

double left_angle = 0.05; //(rad) angle for straigth zone
double right_angle = 0.15; //(rad) angle for bent zone

long store_displacement_count = 0;
