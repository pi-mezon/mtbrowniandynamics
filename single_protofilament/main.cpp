#include "singleprotofilament.h"

#include <perfrunner.h>

#include <getopt.h>

struct SingleProtoPerf : PerfRunner
{
    int singleLaunch() override
    {
        auto start = system_clock::now();
//        setbuf(stdout, NULL);

        SingleProto proto(8, 2000000);
        proto.execute();

        int64_t delta = duration_cast<milliseconds>(system_clock::now() - start).count();
        printf("delta time %ld ms\n", delta);
        return delta;
    }
};

int main(int argc, char **argv)
{
    const option long_opts[] = {
        {"snapshot", required_argument, nullptr, 's'},
        {"type", required_argument, nullptr, 't'},
        {"config", required_argument, nullptr, 'c'},
        {"length", optional_argument, nullptr, 'l'},
        {"help", no_argument, nullptr, 'h'}};

    bool help = false;

    SPLaunchParameters parameters;

    int index_ptr = -1;
    int opt = getopt_long(argc, argv, "s:t:c:l:h", long_opts, &index_ptr);
    while (opt != -1) {
        if (opt == 's')
            parameters.snapshot_path = optarg;
        else if (opt == 't')
            parameters.type = DimerType(optarg[0]);
        else if (opt == 'c')
            parameters.config_path = optarg;
        else if (opt == 'l')
            parameters.length = std::stoi(optarg);
        else if (opt == 'h')
            help = true;
        opt = getopt_long(argc, argv, "s:t:c:l:h", long_opts, &index_ptr);
    }

    if (help) {
        printf("./SingleProto [options]\n");
        printf("\t-c|--config\t\tpath to config with constants\n");
        printf("\t-t|--type\t[T|D]\tdimer type for initial tubule for clean start\n");
        printf("\t-l|--length\t\tlength for clear start\n");
        printf("\t-s|--snapshot\t\tpath to snapshot to start with\n");

        exit(0);
    }

    parameters.launch_prefix = getChronoPrefix();
    assert(dir_exists(parameters.launch_prefix));

    if (!parameters.snapshot_path.empty() && !file_exists(parameters.snapshot_path))
        ret_error("incorrect snapsot path");

    if (parameters.length < 2)
        ret_error("length parameter must be >=2");

    SingleProto proto(parameters);
    proto.execute();

    return 0;
}
