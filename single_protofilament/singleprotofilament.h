#include "singleprotoconstants.h"

#include "forcesphere.h"
#include "protodisplacement.h"

#include <microtubule.h>

struct SPLaunchParameters : LaunchParameters
{
    int length = 8; // in dimers
};

struct AngleZone
{
    AngleZone(size_t proto_number, int mono_number) :
        proto_number(proto_number), mono_number(mono_number) {}

    enum Zone {Left, Right, Center, Undef} prev_zone = Undef;

    void checkZone(const Monomer &lower, const Monomer &upper, int64_t counter)
    {
        assert(lower.proto_number == proto_number &&
               upper.proto_number == proto_number &&
               lower.mono_number + 1 == mono_number &&
               upper.mono_number == mono_number);
        double delta = upper.y_rot_angle - lower.y_rot_angle;

        if (delta < left_angle) { // current zone Left
            ++left;

            if (prev_zone == Right) {
                ++r2l;
                if (!times_r2l.empty())
                    times_r2l.back() = counter - times_r2l.back();
                times_l2r.push_back(counter);
            } else if (prev_zone == Undef && times_l2r.empty()) {
                times_l2r.push_back(counter);
            }
            prev_zone = Left;
        } else if (delta > right_angle) { // current zone Right
            ++right;

            if (prev_zone == Left) {
                ++l2r;
                if (!times_l2r.empty())
                    times_l2r.back() = counter - times_l2r.back();
                times_r2l.push_back(counter);
            } else if (prev_zone == Undef && times_r2l.empty()) {
                times_r2l.push_back(counter);
            }
            prev_zone = Right;
        }
    }

    void clearCounter()
    {
        l2r = 0;
        r2l = 0;
        left = 0;
        right = 0;
        times_l2r.clear();
        times_r2l.clear();

        prev_zone = Undef;
    }

    size_t proto_number;
    int mono_number;

    int64_t l2r = 0;
    int64_t r2l = 0;
    int64_t left = 0;
    int64_t right = 0;
    std::deque<int64_t> times_l2r;
    std::deque<int64_t> times_r2l;
};

struct RadialZone
{
    RadialZone(size_t proto_number, int mono_number) :
        proto_number(proto_number), mono_number(mono_number)
    {}

    enum Zone {Left, Right, Center, Undef} zone = Undef;

    void checkZone(const Monomer &mono) {
        assert(mono.proto_number == proto_number && mono.mono_number == mono_number);

        Point c = mono.center;

        c.rotate_z(mono.minus_sin_z_angle, mono.cos_z_angle);

        Zone current = Undef;

        const double delta = std::abs(mono.center.x - R_MT);

        if (delta < left_zone) {
            current = Left;
            ++left;
        } else if (delta > right_zone) {
            current = Right;
            ++right;
        } else {
            current = Center;
        }

        if (zone == Left && current == Right)
            ++left_to_right;
        else if (zone == Right && current == Left)
            ++right_to_left;

        if (current == Right || current == Left)
            zone = current;
    }

    void clearCounter() {
        left_to_right = 0;
        right_to_left = 0;

        left = 0;
        right = 0;

        zone = Undef;
    }

    size_t proto_number;
    int mono_number;

    int64_t left_to_right = 0;
    int64_t right_to_left = 0;
    int64_t left = 0;
    int64_t right = 0;
};

struct SingleProto
{
    SingleProto(const SPLaunchParameters &param, bool stdout_to_log = true)
    {
        launch_prefix = param.launch_prefix;
        assert(dir_exists(launch_prefix));

        std::string dir_path = launch_prefix + delimiter();

        if (stdout_to_log) {
            log = std::freopen((dir_path + "log.txt").c_str(), "w", stdout);
            setvbuf(log , NULL, _IOLBF, 1024);
        }

        f_snapshot = std::fopen((dir_path + "snapshots.txt").c_str(), "w");
        f_transition = std::fopen((dir_path + "transfers.txt").c_str(), "w");

        SingleProtoConstants::processConstantsFile(param.config_path);
        if (sphere_radius > 0)
            force_sphere.reset(new ForceSphere);


        if (!param.snapshot_path.empty())
            init(param.snapshot_path.c_str());
        else
            init(param.length, param.type);

        if (store_displacement_count > 0) {
            displacement.reset(new ProtoDisplacement(dir_path + "displacement.txt",
                                                     store_displacement_count));
        }
    }

    ~SingleProto()
    {
        if (log)
            fclose(log);

        if (f_snapshot)
            fclose(f_snapshot);

        if (f_transition)
            fclose(f_transition);
    }

    int init(int length, DimerType type = DimerType::D)
    {
        printf("clear start: length %d, type %c\n", length, char(type));

        const double angle = type == DimerType::T ? teta0_T : teta0_D;
        proto.add_dimers_back(1, type);  // first 2 straight monomers
        proto.add_dimers_back(length - 1, type, angle);

        if (force_sphere)
            force_sphere->init(proto);

        printf("initial angles\n");
        for (size_t i = 1; i < proto.monomers.size(); ++i) {
            const auto &mono = proto.monomers[i];
            const auto &prev = proto.monomers[i - 1];
            printf("mono %d angle %f delta %f\n", mono.mono_number, mono.y_rot_angle,
                                                  mono.y_rot_angle - prev.y_rot_angle);
        }

        proto.fill_random_array();

        start = 0;
        finish = start + steps;

        a_zones = {AngleZone(proto.number, first_fixed), AngleZone(proto.number, first_fixed + 1)};

        return 0;
    }

    void init(const char *snapshot_path)
    {
        printf("start from snapshot: %s\n", snapshot_path);

        std::list<std::string> last_snapshot;
        Microtubule::getLastSnapshot(snapshot_path, last_snapshot);

        Monomer *mono = nullptr;
        std::smatch sm;
        int64_t iteration = -1;

        for (auto it = last_snapshot.rbegin(); it != last_snapshot.rend(); ++it) {
            if (std::regex_match(*it, sm, proto_regex)) {
                proto.number = std::stoi(sm[1].str());
                iteration = std::stoll(sm[2].str());
            } else if (std::regex_match(*it, sm, mono_regex)) {
                size_t proto_number = std::stoi(sm[1].str());
                int mono_number = std::stoi(sm[2].str());
                assert(proto.number == proto_number);
                assert(!mono || mono->mono_number + 1 == mono_number);
                proto.monomers.emplace_back(Monomer());
                mono = &proto.monomers.back();
                mono->proto_number = proto_number;
                mono->mono_number = mono_number;
            } else if (std::regex_match(*it, sm, angles_type_regex)) {
                assert(mono);
                mono->y_rot_angle = std::stof(sm[1].str());
                mono->set_z_rot_angle(std::stof(sm[2].str()));
                std::string type = sm[3].str();
                assert(type.size() == 1 &&
                       (type[0] == char(DimerType::D) || type[0] == char(DimerType::T)));
                mono->type = static_cast<DimerType>(type[0]);
            } else if (std::regex_match(*it, sm, name_coord_regex) && sm[1].str() == "center") {
                assert(mono);
                mono->center = Point(sm[2].str());
                mono->update_force_points();
            }
        }
        for (auto &mono : proto.monomers)
            mono.clear_gradient();
        proto.fill_random_array();

        start = iteration;
        finish = start + steps;
    }

    void snapshot(int64_t counter)
    {
        std::fprintf(f_snapshot, "snapshot %ld started\n", counter);
        proto.print_new(f_snapshot, counter);

        if (force_sphere) {
            fprintf(f_snapshot, "\n");
            force_sphere->print(f_snapshot);
        }
        std::fprintf(f_snapshot, "snapshot %ld finished\n", counter);
    }

    void write_transition(int64_t counter)
    {
        std::fprintf(f_transition, "transition %ld\n", counter);
        for (auto &a_z : a_zones) {
            std::fprintf(f_transition, "p %zd m %d l2r %ld r2l %ld l %ld r %ld\n", a_z.proto_number,
                         a_z.mono_number, a_z.l2r, a_z.r2l, a_z.left, a_z.right);

            if (a_z.prev_zone == AngleZone::Right)
                a_z.times_r2l.pop_back();
            else if (a_z.prev_zone == AngleZone::Left)
                a_z.times_l2r.pop_back();

            if (a_z.times_l2r.size() > 1) {
                std::fprintf(f_transition, "times l2r ");
                for (const auto &v : a_z.times_l2r)
                    std::fprintf(f_transition, "%ld ", v);
                std::fprintf(f_transition, "\n");
            }
            if (a_z.times_r2l.size() > 1) {
                std::fprintf(f_transition, "times r2l ");
                for (const auto &v : a_z.times_r2l)
                    std::fprintf(f_transition, "%ld ", v);
                std::fprintf(f_transition, "\n");
            }
        }
        std::fprintf(f_transition, "\n");
    }

    void execute()
    {
        using namespace std::chrono;

        step_polymerisation = kinetic_time_polymerisation / dt;

        printf("start at %ld seconds from epoch\n",
               duration_cast<seconds>(system_clock::now().time_since_epoch()).count());

        int64_t counter = start;
        while(counter < finish) {
            proto.fill_longitudal_gradient();

            if (force_sphere)
                force_sphere->fillInteractionAddition(&proto);

            ++counter;

            proto.move_depolymerise();

            if (force_sphere)
                force_sphere->move();

            if (counter >= write_transfer_offset) {
                for (auto &a_z : a_zones) {
                    a_z.checkZone(proto.monomers[a_z.mono_number - 1],
                            proto.monomers[a_z.mono_number], counter);
                }
            }

            proto.fill_random_array();

            if (!(counter % write_snapshot_rate)) {
                printf("write snapshot %ld\n", counter);
                snapshot(counter);
            }

            if (!(counter % write_transfer_rate) && counter > write_transfer_offset) {
                write_transition(counter);
                for (auto &a_z : a_zones) {
                    a_z.clearCounter();
                    a_z.checkZone(proto.monomers[a_z.mono_number - 1],
                            proto.monomers[a_z.mono_number], counter);
                }
            }

            if(force_sphere && force_sphere->isMaxZExceeded(proto)) {
                printf("max Z exceeded iteration %zd\n", counter);
                break;
            }

            if (!(counter % step_polymerisation)) {
                printf("polymerisation %ld\n", counter);
                proto.polymerisation();
            }

            if (displacement)
                displacement->saveProto(proto, counter);

        }

        printf("finish at %ld seconds from epoch\n",
               duration_cast<seconds>(system_clock::now().time_since_epoch()).count());
    }

    Protofilament proto = Protofilament(0);

    std::unique_ptr<ForceSphere> force_sphere;

    int64_t start;
    int64_t finish;

    int64_t step_polymerisation = kinetic_time_polymerisation / dt;

    std::string launch_prefix;

    std::vector<AngleZone> a_zones;

    std::unique_ptr<ProtoDisplacement> displacement;

    friend class SingleProtoPerf;

    FILE *log = nullptr;
    FILE *f_snapshot = nullptr;
    FILE *f_transition = nullptr;

private:
    SingleProto(int length, int64_t steps)
    {
        init(length);
        finish = start + steps;
    }
};
