#pragma once

#include <stdint.h>

#include <vector>
#include <list>
#include <stdio.h>
#include <string>

struct Monomer;
struct Protofilament;

struct MonoCoords
{
    MonoCoords(const Monomer &mono);
    MonoCoords(int numb = 0, double r = 0, double z = 0);
    MonoCoords(const MonoCoords &) = default;

    int number;
    double r;
    double z;
};

struct ProtoCoords
{
    ProtoCoords(const Protofilament &proto, int64_t iteration);

    int64_t iteration;
    std::list<MonoCoords> monomers;
};

struct ProtoDisplacement
{
    ProtoDisplacement(const std::string &path, int64_t drop_count);

    void saveProto(const Protofilament &proto, int64_t iteration);

    std::list<ProtoCoords> snapshots;
    FILE *out;
    int64_t drop_count = 0;
};
