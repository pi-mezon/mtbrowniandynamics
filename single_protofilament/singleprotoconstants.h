#pragma once

#include "mtconstants.h"

extern long write_transfer_rate;
extern long write_transfer_offset;

extern double left_zone;
extern double right_zone;

extern double left_angle;
extern double right_angle;

extern long store_displacement_count;

struct SingleProtoConstants : MtConstants
{
    static void processConstantsFile(const std::string &file_path)
    {
        SingleProtoConstants constants;

        constants.parseFile(file_path);

        constants.postParseActions();

        constants.printConstants();

        constants.printAdditions();
    }

protected:
    SingleProtoConstants() : MtConstants() {
        ::harmonic_interdimer = true;
    }

private:
    Constant<double> left_zone = {"left_zone", &::left_zone, this};
    Constant<double> right_zone = {"right_zone", &::right_zone, this};
    Constant<double> left_angle = {"left_angle", &::left_angle, this};
    Constant<double> right_angle = {"right_angle", &::right_angle, this};

    Constant<long> write_transfer_rate = {"write_transfer_rate", &::write_transfer_rate, this};
    Constant<long> write_transfer_offset = {"write_transfer_offset", &::write_transfer_offset, this};
    Constant<long> store_displacement_count = {"store_displacement_count", &::store_displacement_count, this};
};
