#include <constants.h>
#include <microtubule.h>

#include <cstdio>

namespace GradientFinder {

struct Monomer
{
    int number;
    size_t proto_number;

    enum Component {Radius, Height, Angle};
    enum DistanceDirection {Up, Down, Left, Right};

    std::map<DistanceDirection, float> distances;
    std::map<Component, float> components_lateral;
    std::map<Component, float> components_longitudal;

    static std::string componentNameByType(const Component &type) {
        if (type == Radius)
            return "Radius";
        else if (type == Height)
            return "Height";
        else if (type == Angle)
            return "Angle";
        else
            return "Undef";
    }

    static std::string directionNameByType(const DistanceDirection &direction) {
        if (direction == Up)
            return "Up";
        else if (direction == Down)
            return "Down";
        else if (direction == Left)
            return "Left";
        else if (direction == Right)
            return "Right";
        else
            return "Undef";
    }
};

struct Protofilament
{
    Protofilament(size_t number, Protofilament *right = nullptr) :
        number(number), right(right) {}

    void connect()
    {
        if (right)
            right->left = this;
    }

    size_t number;
    Protofilament *left = nullptr;
    Protofilament *right = nullptr;

    std::vector<Monomer> monomers;
};

} // GradientFinder

#include <getopt.h>

int main(int argc, char **argv)
{
    const option long_opts[] = {
        {"snapshot", required_argument, nullptr, 's'},
        {"config", required_argument, nullptr, 'c'},
        {"help", no_argument, nullptr, 'h'}};

    bool help = false;

    LaunchParameters parameters;

    int indexPtr = -1;
    int opt = getopt_long(argc, argv, "s:c:h", long_opts, &indexPtr);
    while (opt != -1) {
        if (opt == 's')
            parameters.snapshot_path = optarg;
        else if (opt == 'c')
            parameters.config_path = optarg;
        else if (opt == 'h')
            help = true;
        opt = getopt_long(argc, argv, "s:c:h", long_opts, &indexPtr);
    }

    if (help) {
        printf("./MTRestart [options]\n");
        printf("\t-c|--config\t\tpath to config with constants\n");
        printf("\t-s|--snapshot\t\tpath to snapshot to start with\n");
        printf("\t-h|--help\t\tshow this message\n");

        exit(0);
    }

    if (parameters.snapshot_path.empty())
        ret_error("snapshot path required");

    int pos = parameters.snapshot_path.rfind(delimiter());
    if (pos == -1) {
        parameters.launch_prefix = "./";
    } else {
        parameters.launch_prefix = std::string(parameters.snapshot_path.begin(),
                                               parameters.snapshot_path.begin() + pos);
    }

    Microtubule mt(parameters, false);

    using GradProto = GradientFinder::Protofilament;
    using GradMono = GradientFinder::Monomer;

    std::vector<GradProto> gradient_protofilaments;
    gradient_protofilaments.reserve(mt.protofilaments.size());
    for (size_t c = 0; c < mt.protofilaments.size(); ++c) {
        GradProto *ptr = !gradient_protofilaments.empty() ? &gradient_protofilaments.back() :
                                                            nullptr;
        gradient_protofilaments.emplace_back(GradProto(c, ptr));
        gradient_protofilaments.back().connect();
    }

    gradient_protofilaments[0].right = &gradient_protofilaments[mt.protofilaments.size() - 1];
    gradient_protofilaments[0].connect();

    assert(gradient_protofilaments.size() == mt.protofilaments.size());

    using Component = GradientFinder::Monomer::Component;
    using Direction = GradientFinder::Monomer::DistanceDirection;

    for (size_t c = 0; c < mt.protofilaments.size(); ++c) {
        auto &mt_proto = mt.protofilaments[c];
        auto &gradient_proto = gradient_protofilaments[c];

        mt_proto.fill_lateral_gradient(*mt_proto.left);
        mt_proto.fill_lateral_gradient(*mt_proto.right);
        mt_proto.fill_longitudal_gradient();

        gradient_proto.monomers.reserve(mt_proto.monomers.size());
        for (size_t c = 0; c < mt_proto.monomers.size(); ++c) {
            const Monomer *mono = &mt_proto.monomers[c];

            GradientFinder::Monomer grad_mono;
            grad_mono.number = mono->mono_number;
            grad_mono.proto_number = mono->proto_number;

            auto lat_radius = mono->lateral_gradient_radius_l + mono->lateral_gradient_radius_r;
            auto lat_height = mono->lateral_gradient_height_l + mono->lateral_gradient_height_r;
            auto lat_angle = mono->lateral_gradient_angle_l + mono->lateral_gradient_angle_r;
            grad_mono.components_lateral.insert({Component::Radius, lat_radius});
            grad_mono.components_lateral.insert({Component::Height, lat_height});
            grad_mono.components_lateral.insert({Component::Angle, lat_angle});

            grad_mono.components_longitudal.insert({Component::Radius, mono->longitudal_gradient_radius});
            grad_mono.components_longitudal.insert({Component::Height, mono->longitudal_gradient_heigth});
            grad_mono.components_longitudal.insert({Component::Angle, mono->longitudal_gradient_angle});

            gradient_proto.monomers.push_back(grad_mono);
        }

        for (size_t c = 1; c < mt_proto.monomers.size(); ++c) {
            auto &gradient_lower = gradient_proto.monomers[c - 1];
            auto &gradient_upper = gradient_proto.monomers[c];

            const Monomer &lower = mt_proto.monomers[c - 1];
            const Monomer &upper = mt_proto.monomers[c];

            float distance = upper.down_point.get_distance(lower.up_point);

            gradient_lower.distances.insert({Direction::Up, distance});
            gradient_upper.distances.insert({Direction::Down, distance});
        }
    }

    for (size_t p_counter = 0; p_counter < mt.protofilaments.size(); ++p_counter) {
        const auto &proto = mt.protofilaments.at(p_counter);
        auto &grad_proto = gradient_protofilaments.at(p_counter);

        const Protofilament *left = &proto;
        const Protofilament *right = proto.right;

        GradProto *left_grad = &grad_proto;
        GradProto *right_grad = grad_proto.right;

        int start_left = left->number == 0 && right->number == (number_of_proto - 1) ? 3 : 0;
        int start_right = right->number == 0 && left->number == (number_of_proto - 1) ? 3 : 0;

        size_t interval = std::min(left->monomers.size() - start_left, right->monomers.size() - start_right);
        for (size_t c = 0; c < interval; ++c) {
            const Monomer &left_mono = left->monomers.at(start_left + c);
            const Monomer &right_mono = right->monomers.at(start_right + c);

            GradMono &left_grad_mono = left_grad->monomers.at(start_left + c);
            GradMono &right_grad_mono = right_grad->monomers.at(start_right + c);

            float distance = left_mono.right_force_point.get_distance(right_mono.left_force_point);

            left_grad_mono.distances.insert({Direction::Right, distance});
            right_grad_mono.distances.insert({Direction::Left, distance});
        }
    }

    printf("\n");
    for (const auto &proto : gradient_protofilaments) {
        for (const auto &mono : proto.monomers) {
            printf("protofilament %zd monomer %d\n", proto.number, mono.number);

            for (auto it = mono.distances.begin(); it != mono.distances.end(); ++it)
                printf("\t\t%-8s %8.3f\n", GradMono::directionNameByType(it->first).c_str(), it->second);
            printf("\n");

            printf("\tlateral\n");
            for (auto it = mono.components_lateral.begin(); it != mono.components_lateral.end(); ++it)
                printf("\t\t%-8s %8.3f\n", GradMono::componentNameByType(it->first).c_str(), it->second);
            printf("\n");

            printf("\tlongitudal\n");
            for (auto it = mono.components_longitudal.begin(); it != mono.components_longitudal.end(); ++it)
                printf("\t\t%-8s %8.3f\n", GradMono::componentNameByType(it->first).c_str(), it->second);
        }
    }

    return 0;
}
