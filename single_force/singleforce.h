#include "protofilament.h"
#include "singleforceconstants.h"

struct LaunchParams
{
    std::string launch_prefix;
    std::string config_path;
    size_t length = 8;
    DimerType type = DimerType::T;
};

struct AngleZone
{
    AngleZone(size_t proto_number, int mono_number) :
        proto_number(proto_number), mono_number(mono_number) {}

    enum Zone {Left, Right, Center, Undef} prev_zone = Undef;

    void checkZone(const Monomer &lower, const Monomer &upper, int64_t counter)
    {
        assert(lower.proto_number == proto_number &&
               upper.proto_number == proto_number &&
               lower.mono_number + 1 == mono_number &&
               upper.mono_number == mono_number);
        double delta = upper.y_rot_angle - lower.y_rot_angle;

        if (delta < left_angle) { // current zone Left
            ++left;

            if (prev_zone == Right) {
                ++r2l;
                if (!times_r2l.empty())
                    times_r2l.back() = counter - times_r2l.back();
                times_l2r.push_back(counter);
            } else if (prev_zone == Undef && times_l2r.empty()) {
                times_l2r.push_back(counter);
            }
            prev_zone = Left;
        } else if (delta > right_angle) { // current zone Right
            ++right;

            if (prev_zone == Left) {
                ++l2r;
                if (!times_l2r.empty())
                    times_l2r.back() = counter - times_l2r.back();
                times_r2l.push_back(counter);
            } else if (prev_zone == Undef && times_r2l.empty()) {
                times_r2l.push_back(counter);
            }
            prev_zone = Right;
        }
    }

    void clearCounter()
    {
        l2r = 0;
        r2l = 0;
        left = 0;
        right = 0;
        times_l2r.clear();
        times_r2l.clear();

        prev_zone = Undef;
    }

    size_t proto_number;
    int mono_number;

    int64_t l2r = 0;
    int64_t r2l = 0;
    int64_t left = 0;
    int64_t right = 0;
    std::deque<int64_t> times_l2r;
    std::deque<int64_t> times_r2l;
};

struct SingleForce
{
    SingleForce(const LaunchParams &params);
    ~SingleForce();

    int execute();
    void writeTransition(int64_t counter);

    void snapshot(uint64_t counter) const ;

    Protofilament right = Protofilament(0, nullptr);
    Protofilament center = Protofilament(1, &right);
    Protofilament left = Protofilament(2, &center);

    std::array<Protofilament *, 3> protofilaments = {{&right, &center, &left}};

    uint64_t finish;

    FILE *log = nullptr;
    FILE *f_snapshot = nullptr;
    FILE *f_transition = nullptr;

    std::vector<AngleZone> a_zones;
};
