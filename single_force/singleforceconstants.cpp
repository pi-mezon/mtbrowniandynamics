#include "singleforceconstants.h"

long write_transfer_rate = -1;
long write_transfer_offset = 1e7;

double left_angle = 0.05; //(rad) angle for straigth zone
double right_angle = 0.15; //(rad) angle for bent zone

void SingleForceConstants::processConstantsFile(const std::string &file_path)
{
    SingleForceConstants constants;

    constants.parseFile(file_path);
    constants.postParseActions();
    constants.printConstants();
    constants.printAdditions();
}
