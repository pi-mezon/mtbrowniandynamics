#include "singleviscosity.h"

#include "singleviscosityconstants.h"
#include "util.h"

SingleViscosity::SingleViscosity(const LaunchParams &params)
{
    if (!dir_exists(params.launch_prefix))
        throw std::logic_error("Launch prefix dir not exists");
    log = std::freopen((params.launch_prefix + delimiter() + "log.txt").c_str(), "w", stdout);
    setvbuf(log , NULL, _IOLBF, 1024);
    f_snapshot = std::fopen((params.launch_prefix + delimiter() + "snapshots.txt").c_str(), "w");

    ::dt = params.dt;
    SingleViscosityConstants::processConstantsFile(params.config_path);
    Monomer::lat_cutoff.resize(PairType::END);
    Monomer::lat_cutoff[TT] = {b_lat_T, lateral_cutoff_TT};
    Monomer::lat_cutoff[DD] = {b_lat_D, lateral_cutoff_DD};
    Monomer::lat_cutoff[TD] = {b_lat_TD, lateral_cutoff_TD};
    ::harmonic_interdimer = true;

    left.add_dimers_back(params.length, params.type);
    left.force_sphere.reset();

    right.add_dimers_back(params.length, params.type);
    right.force_sphere.reset();

    center.add_dimers_back(1, params.type);
    center.add_dimers_back(params.length - 1, params.type);

    for (auto &pf : protofilaments) {
        pf->connect();
        pf->fill_random_array();
    }

    finish = 1 + steps;
}

SingleViscosity::~SingleViscosity()
{
    if (log)
        fclose(log);
    if (f_snapshot)
        fclose(f_snapshot);
}

int SingleViscosity::execute()
{
    using namespace std::chrono;

    printf("start at %ld ns from epoch\n",
           duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count());

    uint64_t counter = 1;
    while (counter < finish) {
        center.fill_lateral_gradient(*center.left);
        center.fill_lateral_gradient(*center.right);
        center.fill_longitudal_gradient();

        center.move_depolymerise();
        center.fill_random_array();

        if (T_delta && !(counter % change_rate)) {
            if ((T + T_delta) >= 0)
                T += T_delta;
            dynamic_viscosity = SingleViscosityConstants::viscosityByTemp(T);
            printf("changed %lu T is %f, viscosity is %f\n", counter, T, dynamic_viscosity);
            SingleViscosityConstants::updateMoveCoefficients(dynamic_viscosity);
        }

        if (!(counter % write_snapshot_rate)) {
            printf("write snapshot %lu\n", counter);
            snapshot(counter);
        }
        ++counter;
    }

    printf("finish at %ld ns from epoch\n",
           duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count());

    return 0;
}

void SingleViscosity::snapshot(uint64_t counter) const
{
    if (!f_snapshot)
        return;
    std::fprintf(f_snapshot, "snapshot %lu started\n", counter);
    for (const auto &proto : protofilaments)
        proto->print_new(f_snapshot, counter);
    std::fprintf(f_snapshot, "snapshot %lu finished\n", counter);
}
