#pragma once

#include "mtconstants.h"

extern long write_transfer_rate;
extern long write_transfer_offset;

extern double left_angle;
extern double right_angle;

struct SingleForceConstants : MtConstants
{
    static void processConstantsFile(const std::string &file_path);

private:
    Constant<double> left_angle = {"left_angle", &::left_angle, this};
    Constant<double> right_angle = {"right_angle", &::right_angle, this};

    Constant<long> write_transfer_rate = {"write_transfer_rate", &::write_transfer_rate, this};
    Constant<long> write_transfer_offset = {"write_transfer_offset", &::write_transfer_offset, this};
};
