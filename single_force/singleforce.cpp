#include "singleforce.h"

#include "util.h"

#include <cstdio>

SingleForce::SingleForce(const LaunchParams &params)
{
    if (!dir_exists(params.launch_prefix))
        throw std::logic_error("Launch prefix dir not exists");
    log = std::freopen((params.launch_prefix + delimiter() + "log.txt").c_str(), "w", stdout);
    setvbuf(log , NULL, _IOLBF, 1024);
    f_snapshot = std::fopen((params.launch_prefix + delimiter() + "snapshots.txt").c_str(), "w");

    SingleForceConstants::processConstantsFile(params.config_path);
    Monomer::lat_cutoff.resize(PairType::END);
    Monomer::lat_cutoff[TT] = {b_lat_T, lateral_cutoff_TT};
    Monomer::lat_cutoff[DD] = {b_lat_D, lateral_cutoff_DD};
    Monomer::lat_cutoff[TD] = {b_lat_TD, lateral_cutoff_TD};
    ::harmonic_interdimer = true;

    left.add_dimers_back(params.length, params.type);
    left.force_sphere.reset();

    right.add_dimers_back(params.length, params.type);
    right.force_sphere.reset();

    center.add_dimers_back(1, params.type);
    center.add_dimers_back(params.length - 1, params.type, params.type == DimerType::T ? teta0_T :
                                                                                         teta0_D);
    if (sphere_radius) {
        center.force_sphere.reset(new ForceSphere());
        center.force_sphere->init(center);
    }

    for (auto &pf : protofilaments) {
        pf->connect();
        pf->fill_random_array();
    }

    finish = 1 + steps;

    if (write_transfer_rate > 0) {
        a_zones = {AngleZone(center.number, first_fixed), AngleZone(center.number, first_fixed + 1)};
        f_transition = std::fopen((params.launch_prefix + delimiter() + "transitions.txt").c_str(), "w");
    }
}

SingleForce::~SingleForce()
{
    if (log)
        fclose(log);
    if (f_snapshot)
        fclose(f_snapshot);
    if (f_transition)
        fclose(f_transition);
}

int SingleForce::execute()
{
    using namespace std::chrono;

    printf("start at %ld ns from epoch\n",
           duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count());

    uint64_t counter = 1;
    while (counter < finish) {
        center.fill_lateral_gradient(*center.left);
        center.fill_lateral_gradient(*center.right);
        if (center.force_sphere)
            center.force_sphere->fillInteractionAddition(&center);
        center.fill_longitudal_gradient();

        center.move_depolymerise();
        center.fill_random_array();

        if (!a_zones.empty() && counter > (unsigned long)write_transfer_rate) {
            for (auto &a_z : a_zones) {
                a_z.checkZone(center.monomers[a_z.mono_number - 1],
                        center.monomers[a_z.mono_number], counter);
            }
            if (!(counter % write_transfer_rate)) {
                writeTransition(counter);
                for (auto &a_z : a_zones)
                    a_z.clearCounter();
            }
        }

        if (!(counter % write_snapshot_rate)) {
            printf("write snapshot %lu\n", counter);
            snapshot(counter);
        }

        if (center.force_sphere && center.force_sphere->isMaxZExceeded(center)) {
            printf("max Z exceeded iteration %zd\n", counter);
            break;
        }
        ++counter;
    }

    printf("finish at %ld ns from epoch\n",
           duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count());

    return 0;
}

void SingleForce::writeTransition(int64_t counter)
{
    if (a_zones.empty() || !f_transition)
        return;
    std::fprintf(f_transition, "transition %ld\n", counter);
    for (auto &a_z : a_zones) {
        std::fprintf(f_transition, "p %zd m %d l2r %ld r2l %ld l %ld r %ld\n", a_z.proto_number,
                     a_z.mono_number, a_z.l2r, a_z.r2l, a_z.left, a_z.right);
        if (a_z.prev_zone == AngleZone::Right)
            a_z.times_r2l.pop_back();
        else if (a_z.prev_zone == AngleZone::Left)
            a_z.times_l2r.pop_back();

        if (a_z.times_l2r.size() > 1) {
            std::fprintf(f_transition, "times l2r ");
            for (const auto &v : a_z.times_l2r)
                std::fprintf(f_transition, "%ld ", v);
            std::fprintf(f_transition, "\n");
        }
        if (a_z.times_r2l.size() > 1) {
            std::fprintf(f_transition, "times r2l ");
            for (const auto &v : a_z.times_r2l)
                std::fprintf(f_transition, "%ld ", v);
            std::fprintf(f_transition, "\n");
        }
    }
    std::fprintf(f_transition, "\n");
}

void SingleForce::snapshot(uint64_t counter) const
{
    std::fprintf(f_snapshot, "snapshot %lu started\n", counter);
    for (const auto &proto : protofilaments) {
        proto->print_new(f_snapshot, counter);
        if (proto->force_sphere) {
            fprintf(f_snapshot, "\n");
            proto->force_sphere->print(f_snapshot);
        }
    }
    std::fprintf(f_snapshot, "snapshot %lu finished\n", counter);
}
