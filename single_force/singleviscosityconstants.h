#pragma once

#include "mtconstants.h"

extern double T_delta;

extern long change_rate;

struct SingleViscosityConstants : MtConstants
{
    static void processConstantsFile(const std::string &file_path);

    static double viscosityByTemp(double temp)
    {
        constexpr double n0 = 1.38e-4; // Pa*s
        constexpr double Ts = 225.6; // K, -48 C

        constexpr double n_max = 1e5; // Pa*s

        if (temp <= Ts)
            return n_max;

        double result = n0 * std::pow(temp / Ts - 1, -1.64);
        return result <= n_max ? result : n_max;
    }

    static void updateMoveCoefficients(double viscosity)
    {
        ::gradient_transition_coefficient = K_transition_gradient * dt / (rad_monomer * viscosity);
        ::stochastic_transition_coefficient = K_transition_stochastic * std::sqrt(::dt * ::T / (rad_monomer * viscosity));
        ::gradient_rotation_coefficient = K_rotation_gradient * dt / (std::pow(rad_monomer, 3) * viscosity);
        ::stochastic_rotation_coefficient = K_rotation_stochastic * std::sqrt(::T * dt / (std::pow(rad_monomer, 3) * viscosity));
    }

    Constant<double> T_delta = {"T_delta", &::T_delta, this};

    Constant<long> change_interval = {"change_rate", &::change_rate, this};
};
