#include "singleviscosity.h"
#include "util.h"

#include <getopt.h>

int main(int argc, char **argv)
{
    const option long_opts[] = {
        {"type", optional_argument, nullptr, 't'},
        {"config", optional_argument, nullptr, 'c'},
        {"length", optional_argument, nullptr, 'l'},
        {"dt", optional_argument, nullptr, 0},
        {"help", no_argument, nullptr, 'h'}};

    LaunchParams parameters;

    bool help = false;

    int index_ptr = -1;
    int opt = getopt_long(argc, argv, "t:c:l:h", long_opts, &index_ptr);
    while (opt != -1) {
        if (opt == 't') {
            auto type = DimerType(optarg[0]);
            if (type != DimerType::T && type != DimerType::D)
                ret_error("wrong dimer type");
            parameters.type = DimerType(optarg[0]);
        } else if (opt == 'c') {
            parameters.config_path = optarg;
        } else if (opt == 'l') {
            long v = std::atol(optarg);
            if (v < 1)
                ret_error("Only length >=1 allowed");
            parameters.length = v;
        } else if (opt == 'h') {
            help = true;
        } else if (opt == 0 && !strcmp("dt", long_opts[index_ptr].name)) {
            if (!optarg) {
                if (optind > 0 && optind < argc && argv[optind] != nullptr &&
                        argv[optind][0] != '-')
                    optarg = argv[optind];
            }
            if (!optarg)
                ret_error("Missing option value for dt option");

            try {
                double mb_dt = std::stod(optarg);
                if (mb_dt > 1e-10 || mb_dt < 1e-13)
                    ret_error("dt range [1e-13, 1e-10");
                parameters.dt = mb_dt;
            } catch (const std::exception &e)
            {
                ret_error(std::string("Can't convert dt parameter: ") + std::string(e.what()));
            }
        }
        opt = getopt_long(argc, argv, "t:c:l:h", long_opts, &index_ptr);
    }

    if (help) {
        printf("./SingleForce [options]\n"
               "\t-t|--type\t [T|D]\tdimer type\n"
               "\t-c|--config\t\tpath to config with constants\n"
               "\t-l|--length\t\tPF length\n");
        exit(0);
    }
    parameters.launch_prefix = getChronoPrefix();
    assert(dir_exists(parameters.launch_prefix));

    SingleViscosity o(parameters);
    return o.execute();
}
