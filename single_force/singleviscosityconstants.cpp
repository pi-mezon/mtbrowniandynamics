#include "singleviscosityconstants.h"

double T_delta = 0;

long change_rate = 1000;

void SingleViscosityConstants::processConstantsFile(const std::string &file_path)
{
    SingleViscosityConstants constants;

    constants.parseFile(file_path);
    constants.postParseActions();

    if (::T_delta > 0)
        throw std::logic_error("Positive T_delta not allowed");

    ::dynamic_viscosity = viscosityByTemp(::T);
    updateMoveCoefficients(::dynamic_viscosity);

    constants.printConstants();
    constants.printAdditions();
}
