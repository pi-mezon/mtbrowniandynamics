#pragma once

#include "protofilament.h"

struct LaunchParams
{
    std::string launch_prefix;
    std::string config_path;
    size_t length = 8;
    DimerType type = DimerType::T;
    double dt = 1e-12;
};

struct SingleViscosity
{
    SingleViscosity(const LaunchParams &params);
    ~SingleViscosity();

    int execute();

    void snapshot(uint64_t counter) const;

    Protofilament right = Protofilament(0, nullptr);
    Protofilament center = Protofilament(1, &right);
    Protofilament left = Protofilament(2, &center);

    std::array<Protofilament *, 3> protofilaments = {{&right, &center, &left}};

    uint64_t finish;

    FILE *log = nullptr;
    FILE *f_snapshot = nullptr;
};
