#pragma once

#include "util.h"

#include <assert.h>
#include <cmath>
#include <cstddef>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

class Constants;

struct ConstInterface
{
    virtual std::string state() const = 0;
    virtual std::string name() const = 0;
    virtual std::string strValue() const = 0;
    virtual bool fromConfig() const = 0;
};

template <typename T>
struct Constant : ConstInterface
{
    Constant() = default;
    Constant(const std::string &name, T *var, Constants *constants);

    void set_value(const T &value)
    {
        *value_ptr = value;
        m_config = true;
    }

    bool fromConfig() const { return m_config;}
    std::string state() const override {return m_config ? "config" : "default";}
    T value() const {return *value_ptr;}
    std::string name() const override {return m_name;}
    std::string strValue() const override {
        std::ostringstream out;
        const auto abs_value = std::abs(value());
        if (abs_value >= 1e6 || (abs_value <= 1e-6 && abs_value != 0))
            out << std::scientific;
        out << value();
        return out.str();
    }

protected:
    std::string m_name;
    T *value_ptr = nullptr;
    bool m_config = false;
};

template <>
struct Constant<bool> : ConstInterface
{
    Constant() = default;
    Constant(const std::string &name, bool *var, Constants *constants);

    void set_value(const bool &value)
    {
        *value_ptr = value;
        m_config = true;
    }

    bool fromConfig() const override {return m_config;}
    std::string state() const override {return m_config ? "config" : "default";}
    bool value() const {return *value_ptr;}
    std::string name() const override {return m_name;}
    std::string strValue() const override {return *value_ptr ? "true" : "false";}

protected:
    std::string m_name;
    bool *value_ptr = nullptr;
    bool m_config = false;
};

template <typename T = int>
struct DependentConstant : Constant<T>
{
    enum Operation {Multiplication = '*', Division = '/'} current_action;

    DependentConstant() = default;
    DependentConstant(const std::string &name, const std::string &operation, T *var, Constants *c);

    int processSubstring(const std::string &str);

    void makeOperationConstants(T &res, const ConstInterface *constant, Operation op) const;
    void makeOperation(T &res, size_t pos, Operation op) const;

    void fill() {
        std::set<char> delimiters = {Multiplication, Division};

        auto begin = operation.begin();
        for (auto it = operation.begin(); it != operation.end(); ++it) {
            if (delimiters.count(*it)) {
                processSubstring(std::string(begin, it));
                operation_order.push_back(static_cast<Operation>(*it));
                begin = it + 1;
            }
        }
        processSubstring(std::string(begin, operation.end()));

        assert((constants_order.size() + doubles_order.size()) == (operation_order.size() + 1));

        T res = 1;
        makeOperation(res, 0, Multiplication);
        for (size_t i = 0; i < operation_order.size(); ++i)
            makeOperation(res, i + 1, operation_order[i]);

        Constant<T>::set_value(res);
    }

    std::string state() const override
    {
        std::set<std::string> _config;
        std::set<std::string> _default;
        for (const auto &pair : constants_order) {
            const ConstInterface *p = pair.second;
            p->fromConfig() ? _config.insert(p->name()) : _default.insert(p->name());
        }

        std::string ret;

        if (!_config.empty()) {
            ret += "config: ";
            for (const auto &s : _config)
                ret += s + " ";
        }

        if (!_default.empty()) {
            ret += "default: ";
            for (const auto &s : _default)
                ret += s + " ";
        }

        return ret;
    }

protected:
    const Constants *parent = nullptr;
    std::string operation;

    std::vector<Operation> operation_order;
    std::map<size_t, ConstInterface *> constants_order;
    std::map<size_t, double> doubles_order;
    size_t pos = 0;
};

class Constants
{
protected:
    Constants() {}

    void add_constant(Constant<long> *constant) {
        long_constants.insert(std::make_pair(constant->name(), constant));
    }

    void add_constant(Constant<double> *constant) {
        double_constants.insert(std::make_pair(constant->name(), constant));
    }

    void add_constant(Constant<bool> *constant) {
        bool_constants.insert(std::make_pair(constant->name(), constant));
    }

    void add_constant(DependentConstant<long> *constant) {
        dependent_long_constants.insert(std::make_pair(constant->name(), constant));
    }

    void add_constant(DependentConstant<double> *constant) {
        dependent_double_constants.insert(std::make_pair(constant->name(), constant));
    }

    void parseFile(const std::string &file_path);
    void printConstants();

    template<typename T>
    friend class Constant;
    template<typename T>
    friend class DependentConstant;

private:
    std::map<std::string, Constant<long> *> long_constants;  //todo: check name intersection
    std::map<std::string, Constant<double> *> double_constants;
    std::map<std::string, Constant<bool> *> bool_constants;

    std::map<std::string, DependentConstant<long> *> dependent_long_constants;
    std::map<std::string, DependentConstant<double> *> dependent_double_constants;

    static const std::set<std::string> affirmatives;
    static const std::set<std::string> negatives;
};

template <typename T>
Constant<T>::Constant(const std::string &name, T *var, Constants *constants) :
    m_name(name), value_ptr(var)
{
    if (!var) {
        printf("name %s\n", name.c_str());
        ret_error("constant: invalid variable");
    }

    constants->add_constant(this);
}

template <typename T>
DependentConstant<T>::DependentConstant(const std::string &name, const std::string &operation,
                                        T *var, Constants *c) :
    Constant<T>(name, var, c), parent(c), operation(operation)
{
    c->add_constant(this);
}

template <typename T>
int DependentConstant<T>::processSubstring(const std::string &str)
{
    assert(parent);

    try {
        double v = std::stof(str);
        doubles_order.insert(std::make_pair(pos, v));
    } catch (std::invalid_argument &/*ex*/) {
        const auto &int_constants = parent->long_constants;
        const auto &double_constants = parent->double_constants;
        auto int_it = int_constants.find(str);
        auto double_it = double_constants.find(str);
        bool int_end = int_it == int_constants.end();
        bool double_end = double_it == double_constants.end();
        if ((int_end && double_end) || (!int_end && !double_end))
            return EINVAL;

        if (!int_end)
            constants_order.insert({pos, int_it->second});
        if (!double_end)
            constants_order.insert({pos, double_it->second});
    }

    ++pos;
    return 0;
}

template <typename T>
void DependentConstant<T>::makeOperationConstants(T &res, const ConstInterface *constant, Operation op) const
{
    auto double_ptr = dynamic_cast<const Constant<double> *>(constant);
    auto int_ptr = dynamic_cast<const Constant<int> *>(constant);
    assert((double_ptr || int_ptr) && !(double_ptr && int_ptr));

    if (double_ptr) {
        double value = double_ptr->value();
        if (op == Multiplication) {
            res *= value;
        } else if (op == Division) {
            assert(value != 0);
            res /= value;
        }
    } else if (int_ptr) {
        int value = int_ptr->value();
        if (op == Multiplication) {
            res *= value;
        } else if (op == Division) {
            assert(value != 0);
            res /= value;
        }
    } else {
        assert(false);
    }
}

template <typename T>
void DependentConstant<T>::makeOperation(T &res, size_t pos, Operation op) const
{
    auto double_it = doubles_order.find(pos);
    auto const_it = constants_order.find(pos);
    if (double_it != doubles_order.end()) {
        if (op == Multiplication) {
            res *= double_it->second;
        } else {
            double value = double_it->second;
            assert(value != 0);
            res /= value;
        }
    } else if (const_it != constants_order.end()) {
        makeOperationConstants(res, const_it->second, op);
    } else {
        assert(false);
    }
}
