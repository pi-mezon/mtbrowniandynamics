#include "microtubule.h"

#include <sys/types.h>
#include <sys/stat.h>

#ifdef _WIN32
#include <Windows.h>
#endif

bool dir_exists(const std::string &path) { //todo: check in WIN
    struct stat info;
    if (stat(path.c_str(), &info) != 0) // can't access
        return false;
    return S_ISDIR(info.st_mode);
}

bool file_exists(const std::string &path) {
    struct stat info;
    if (stat(path.c_str(), &info) != 0) // can't access
        return false;

    return S_ISREG(info.st_mode);
}

bool make_dir(const std::string &path) {
#ifdef __linux__
	return !mkdir(path.c_str(), 0700);
#elif _WIN32
	return CreateDirectory(path.c_str(), NULL);
#endif
	return false;
}
