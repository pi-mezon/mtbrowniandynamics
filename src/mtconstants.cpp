#include "mtconstants.h"

double r0_long = 0.13; //width of energy potential well
double r0_lat = 0.13; //width of energy potential well
double a_lat = 8.99;  //corresponds approximately to 2x height of the energy barrier for a lateral bond
double b_lat_T = 4.843;  //depth of the energy potential well for a lateral bond
double b_lat_D = 4.843;  //depth of the energy potential well for a lateral bond
double a_long = 9.0596; //corresponds approximately to 2x height of the energy barrier for longitudinal interdimer bond
double b_long = 8.99; //depth of the potential well for longitudinal interdimer bond
double k_coeff = 300; //stiffness of longitudinal intradimer bond
double b_coeff_D = 174; //flexural rigidity of protofilament bending for mono in D dimer
double b_coeff_T = 174; //flexural rigidity of protofilament bending for mono in T dimer

double teta0_D = 0.2; //equilibrium angle between longitudinally attached GDP-tubulin
double teta0_T = 0.0; //equilibrium angle between longitudinally attached GTP-tubulin
double concentration = 10.0; //soluble tubulin concentration, uM
double K_on = 8.3; //on-rate constant for tubulin addition per MT, (uM*s)^-1
double K_hydrolysis =  0.5; //rate of GTP hydrolysis, s^-1
double T = 310.0; //temperature, K
double dynamic_viscosity = 0.159; // Pa * s

const double K_transition_gradient = 3.69e5; //non-dimensional
const double K_transition_stochastic = 38.27; //non-dimensional
const double K_rotation_gradient = 2.77e5; //non-dimensional
const double K_rotation_stochastic = 33.1; //non-dimensional

double dt = 1e-10; // time step for dynamic algorithm

double kinetic_time_polymerisation = 0.013f; //time step for kinetic algorithm, polymerization
double kinetic_time_hydrolysis = 0.001f; //time step for kinetic algorithm, hydrolysis

double b_lat_TD = (b_lat_T + b_lat_D) / 2;

double r0_long_pow2 = r0_long * r0_long;
double r0_lat_pow2 = r0_lat * r0_lat;

double lateral_cutoff_TT = 1;
double lateral_cutoff_DD = 1;
double lateral_cutoff_TD = 1;
double lateral_cutoff_level = 0.58 * 0.1;

double polymerisation_probability = concentration * K_on * kinetic_time_polymerisation / 13; //Probability per protofilament
double hydrolysis_probability = K_hydrolysis * kinetic_time_hydrolysis;

double depoly_cutoff = 1.f;
double depoly_cutoff_level = 0.58 * 0.1;

const double R_MT = 8.128; // rename
const double rad_monomer = 2.0;
const double pi = 3.141592653589793; //todo: mb use M_PI from math.h ?
const int N_d = 50;

double gradient_transition_coefficient = K_transition_gradient * dt / (rad_monomer * dynamic_viscosity);
double stochastic_transition_coefficient = K_transition_stochastic * std::sqrt(dt * T / (rad_monomer * dynamic_viscosity));
double gradient_rotation_coefficient = K_rotation_gradient * dt / (std::pow(rad_monomer, 3) * dynamic_viscosity);
double stochastic_rotation_coefficient = K_rotation_stochastic * std::sqrt(T * dt / (std::pow(rad_monomer, 3) * dynamic_viscosity));

long steps = 1e9;
long update_frame_rate = 1e6;
long write_snapshot_rate = 1e7;
long check_overcurled_rate = 1e10;
long check_sphere_rate = 1e6;

bool harmonic_interdimer = false;

double sphere_x = 10; // nm, distance between mono center in MT wall and sphere center
double sphere_radius = 0; // nm, force sphere radius; if 0 - without sphere
double repulsion_coeff = 100.; // todo: value and dimension
double force = 1e-12; // N, force

double gradient_transition_coefficient_sphere = K_transition_gradient * dt / (sphere_radius * dynamic_viscosity);
double stochastic_transition_coefficient_sphere = K_transition_stochastic * std::sqrt(dt * T / (sphere_radius * dynamic_viscosity));

double sphere_max_z = 200; // nm, max sphere coord
double K_transition_constant_force = 5.3e16; // non-dimensional
double sphere_constant_force_addition = ::K_transition_constant_force * ::dt * ::force / (::sphere_radius * ::dynamic_viscosity); // 2.7*10^7 *force
double sphere_stop_distance = 100; // nm

/////////////////////////////////////////////////////////////////////////////////////////////
const double phi_right = 1.81245730015; //corect angle
const double theta_right = 1.80376369679;

const double phi_left = -1.81245730015; //corect angle
const double theta_left = 1.3378289568;

const size_t number_of_proto = 13;

const double frame_delta = 0.3f;
const int min_free_length = 4;
const int first_fixed = 2;

double energy(double r, double a_coeff, double b_coeff, double r0)
{
    return a_coeff * std::pow(r / r0, 2) * std::exp(- r / r0) -
            b_coeff * std::exp(- r * r / (r0 * r0));
}
