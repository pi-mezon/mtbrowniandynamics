#pragma once

#include "point.h"
#include "randomgenerator.h"

struct Protofilament;

struct ForceSphere
{
    ForceSphere() = default;
    ForceSphere(const ForceSphere &other) = default;

    void init(const Protofilament &proto);
    void fillInteractionAddition(Protofilament *proto);
    void move();

    void clearGradient()
    {
        gradient_radius = 0.f;
        gradient_heigth = 0.f;
    }

    void print(FILE *dest);

    bool isMaxZExceeded(const Protofilament &proto);

    void updateFromRadii() {
        center.x = radii * cos_z_angle;
        center.y = radii * sin_z_angle;
    }

    double gradient_radius = 0.f;
    double gradient_heigth = 0.f;

    std::shared_ptr<Random> generator;

    Point center;
    double radii = 0;
    int64_t interaction_counter = 0;

    double cos_z_angle = 0;
    double sin_z_angle = 0;
};
