#include "forcesphere.h"

#include "protofilament.h"

#include <math.h>

void ForceSphere::init(const Protofilament &proto)
{
    const auto mono = proto.monomers.front();

    radii = mono.radii + sphere_x;
    cos_z_angle = mono.cos_z_angle;
    sin_z_angle = mono.sin_z_angle;
    updateFromRadii();
    center.z = mono.center.z;

    generator = proto.gen;
}

void ForceSphere::fillInteractionAddition(Protofilament *proto)
{
    double cutoff = rad_monomer + sphere_radius;
    bool interacted = false;

    for (Monomer &mono : proto->monomers) {
        const double distance =  mono.center.get_distance(center);

        if (distance > cutoff)
            continue;

        interacted = true;

        //warning: only for single proto, where x === r
        const double delta_r = radii - mono.radii;
        const double delta_z = center.z - mono.center.z;

        const double common_part = repulsion_coeff * (distance - cutoff) / distance;

        const double sphere_gradient_radius = common_part * delta_r;
        gradient_radius += sphere_gradient_radius;
        mono.force_sphere_gradient_radius -= sphere_gradient_radius;

        const double sphere_gradient_height = common_part * delta_z;
        gradient_heigth += sphere_gradient_height;
        mono.force_sphere_gradient_heigth -= sphere_gradient_height;
    }

    if (interacted)
        ++interaction_counter;
}

void ForceSphere::move()
{
    omp_set_lock(&generator->lock);

    //    x not changed
    //    center.x += - gradient_radius * gradient_transition_coefficient_sphere +
    //            stochastic_transition_coefficient_sphere * generator->Normal_dist();

    center.z += - gradient_heigth * gradient_transition_coefficient_sphere +
            stochastic_transition_coefficient_sphere * generator->Normal_dist() +
            sphere_constant_force_addition;

    omp_unset_lock(&generator->lock);

    updateFromRadii();

    clearGradient();
}

void ForceSphere::print(FILE *dest)
{
    fprintf(dest, "\tforce sphere center %s radius %f\n", center.print().c_str(), sphere_radius);
    fprintf(dest, "\tforce sphere %ld\n", interaction_counter);
}

bool ForceSphere::isMaxZExceeded(const Protofilament &proto)
{
    return center.z - proto.monomers.front().center.z > sphere_max_z;
}
