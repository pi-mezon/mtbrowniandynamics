#pragma once

#include "protofilament.h"
#include "regexes.h"

#include "util.h"
#include "printversion.h"

#include <algorithm>
#include <omp.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <cstring>

struct LaunchParameters
{
    std::string launch_prefix;
    std::string snapshot_path;
    std::string config_path;
    DimerType type = DimerType::T;
};

struct Microtubule
{
    Microtubule(const LaunchParameters &param, bool stdout_to_log = true)
    {
        launch_prefix = param.launch_prefix;
        assert(dir_exists(launch_prefix));

        if (stdout_to_log) {
            log = std::freopen((launch_prefix + delimiter() + "log.txt").c_str(), "w",stdout);
            setvbuf(log , NULL, _IOLBF, 1024);
        }

        //todo: check extend from snapshot case
        f_snapshot = std::fopen((launch_prefix + delimiter() + "snapshots.txt").c_str(), "w");

        printVersion();

        MtConstants::processConstantsFile(param.config_path);

        // check test consistency after change
        Monomer::lat_cutoff.resize(END); //fill after constants parsing
        Monomer::lat_cutoff[TT] = {b_lat_T, lateral_cutoff_TT};
        Monomer::lat_cutoff[DD] = {b_lat_D, lateral_cutoff_DD};
        Monomer::lat_cutoff[TD] = {b_lat_TD, lateral_cutoff_TD};

        if (!param.snapshot_path.empty())
            init(param.snapshot_path.c_str());
        else
            init(6, param.type);
    }

    Microtubule(int length, int64_t steps = 10000) // for tests
    {
        MtConstants::processConstantsFile("");

        Monomer::lat_cutoff.resize(END); //fill after constants parsing
        Monomer::lat_cutoff[TT] = {b_lat_T, lateral_cutoff_TT};
        Monomer::lat_cutoff[DD] = {b_lat_D, lateral_cutoff_DD};
        Monomer::lat_cutoff[TD] = {b_lat_TD, lateral_cutoff_TD};

        ::steps = steps;
        init(length);
    }

    Microtubule(const char *name) // for tests
    {
        init(name);
    }

    void init(int length, DimerType type = DimerType::T)
    {
        printf("clear start: length %d, type %c\n", length, char(type));
        if (!launch_prefix.empty())
            printf("prefix for this launch is %s\n", launch_prefix.c_str());

        protofilaments.reserve(number_of_proto);
        for (size_t c = 0; c < number_of_proto; ++c) {
            Protofilament *ptr = protofilaments.empty() ? nullptr : &protofilaments.back();
            protofilaments.emplace_back(Protofilament(c, ptr));
            protofilaments.back().add_dimers_back(length, type);
            protofilaments.back().connect();

            protofilaments.back().fill_random_array();
        }

        protofilaments[0].right = &protofilaments[12];
        protofilaments[0].connect();

        for (auto &proto : protofilaments) {
            if (proto.force_sphere)
                proto.force_sphere->init(proto);
        }

        start = 1;
        finish = start + steps;
    }

    static void getLastSnapshot(const char *snapshot_path, std::list<std::string> &last_snapshot)
    {
        FILE *f = std::fopen(snapshot_path, "rb");

        std::fseek(f, 0, SEEK_END);
        int64_t remains = std::ftell(f);

        bool found_started = false;
        std::smatch sm;

        int buf_size = 256;
        using Buff = std::vector<char>;
        Buff buf(256, 0);

        while (remains > 0 && !found_started) {
            if (remains < 256)
                printf("here\n");

            int64_t to_read;
            if (remains >= buf_size) {
                to_read = buf_size;
            } else {
                to_read = remains;
                buf.resize(remains);
            }

            std::fseek(f, -1 * to_read, SEEK_CUR);
            if (int64_t(std::fread(buf.data(), 1, to_read, f)) != to_read)
                return ret_error("Error during snapshot reading");

            std::fseek(f, -1 * to_read, SEEK_CUR); // returning to position before read

            std::vector<std::string> splitted = split<Buff>(buf, "\n");
            if (!last_snapshot.empty() && buf.back() != '\n') {
                //merge last string separated by buf, not new line
                last_snapshot.back() = splitted.back() + last_snapshot.back();
                splitted.pop_back();
            }
            std::copy(splitted.rbegin(), splitted.rend(), std::back_inserter(last_snapshot));

            int to_check = splitted.size();
            auto it = --last_snapshot.end();
            for (int c = 0; c < to_check; ++c) {
                if (std::regex_match(*it, sm, snapshot_started)) {
                    last_snapshot.erase(++it, last_snapshot.end());
                    found_started = true;
                    break;
                }
                --it;
            }
            remains -= to_read;
        }
        fclose(f);
    }

    void init(const char *snapshot_path)
    {
        printf("start from snapshot: %s\n", snapshot_path);

        std::list<std::string> last_snapshot;
        getLastSnapshot(snapshot_path, last_snapshot);

        Protofilament *proto = nullptr;
        Monomer *mono = nullptr;
        protofilaments.reserve(number_of_proto);
        std::smatch sm;
        int64_t iteration = -1;

        for (auto it = last_snapshot.rbegin(); it != last_snapshot.rend(); ++it) {
            if (std::regex_match(*it, sm, proto_regex)) {
                int number = std::stoi(sm[1].str());

                if (iteration == -1)
                    iteration = std::stoll(sm[2].str());
                else
                    assert(iteration == std::stoll(sm[2].str()));
                protofilaments.emplace_back(Protofilament(number, proto));
                proto = &protofilaments.back();
                proto->connect();
                mono = nullptr;
            } else if (std::regex_match(*it, sm, mono_regex)) {
                size_t proto_number = std::stoi(sm[1].str());
                int mono_number = std::stoi(sm[2].str());
                assert(proto && proto->number == proto_number);
                assert(!mono || mono->mono_number + 1 == mono_number);
                proto->monomers.emplace_back(Monomer());
                mono = &proto->monomers.back();
                mono->proto_number = proto_number;
                mono->mono_number = mono_number;
            } else if (std::regex_match(*it, sm, angles_type_regex)) {
                assert(mono);
                mono->y_rot_angle = std::stof(sm[1].str());
                mono->set_z_rot_angle(std::stof(sm[2].str()));
                std::string type = sm[3].str();
                assert(type.size() == 1 && (type == "D" || type == "T"));
                mono->type = static_cast<DimerType>(type[0]);
            } else if (std::regex_match(*it, sm, name_coord_regex) && sm[1].str() == "center") {
                assert(mono);
                mono->center = Point(sm[2].str());
                mono->radii = mono->center.x * mono->cos_z_angle +
                        mono->center.y * mono->sin_z_angle;
                mono->update_center();
                mono->update_force_points();
            } else if (std::regex_match(*it, sm, radii_regex)) {
                assert(mono);
                mono->radii = std::atof(sm[1].str().c_str());
                mono->update_center();
                mono->update_force_points();
            }
        }

        protofilaments[0].right = &protofilaments[12];
        protofilaments[0].connect();

        for (auto &proto : protofilaments) {
            for (auto &mono : proto.monomers)
                mono.clear_gradient();

            proto.fill_random_array();

            // todo: get force sphere position from snapshot
            if (proto.force_sphere)
                proto.force_sphere->init(proto);
        }

        if (sphere_radius) {
            double min_z = std::numeric_limits<double>::max();

            for (const auto &proto : protofilaments)
                min_z = std::min(proto.force_sphere->center.z, min_z);

            for (auto &proto : protofilaments)
                proto.force_sphere->center.z = min_z;
        }

        start = iteration;
        finish = start + steps;
    }

    ~Microtubule()
    {
        if (log)
            fclose(log);

        if (f_snapshot)
            fclose(f_snapshot);
    }

    bool equal(const Microtubule &other) {
        if (protofilaments.size() != other.protofilaments.size())
            return false;

        for (size_t c = 0; c < protofilaments.size(); ++c) {
            if (!protofilaments[c].equal(other.protofilaments[c]))
                return false;
        }

        return true;
    }

    bool correct_connected()
    {
        for (const auto &proto : protofilaments) {
            if (!proto.correct_connected())
                return false;
        }
        return true;
    }

    void print_new(FILE *dest, int64_t counter = -1) const
    {
        for (const auto &proto : protofilaments) {
            printf("\t proto %zd monomers count %zd\n", proto.number, proto.monomers.size());
            proto.print_new(dest, counter);

            if (proto.force_sphere) {
                fprintf(dest, "\n");
                proto.force_sphere->print(dest);
            }
        }
    }

    void execute() {
        using namespace std::chrono;

        step_polymerisation = kinetic_time_polymerisation / dt;
        step_hydrolysis = kinetic_time_hydrolysis / dt;

        printf("start at %ld ns from epoch\n",
               duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count());

        for (auto &proto : protofilaments)
            tasks_gradient.emplace_back(&proto, Task::LateralR);
        for (auto &proto : protofilaments)
            tasks_gradient.emplace_back(&proto, Task::Longitudal);
        for (auto &proto : protofilaments)
            tasks_gradient.emplace_back(&proto, Task::LateralL);

        if (sphere_radius) {
            for (auto &proto : protofilaments)
                tasks_gradient.emplace_back(&proto, Task::SphereInteract);
        }

        for (auto &proto : protofilaments)
            tasks_move.emplace_back(&proto, Task::MoveDepolymerize);
        for (auto &proto : protofilaments)
            tasks_move.emplace_back(&proto, Task::FillRandom);

        omp_set_num_threads(omp_get_num_procs());

        int64_t counter = 0;
        bool running = true;

#pragma omp parallel private(counter)
        {

#pragma omp single
            printf("thread numb %d\n", omp_get_num_threads());

            counter = start;
            while (running && counter < finish) {
#pragma omp for
                for (long c = 0; c < long(tasks_gradient.size()); ++c)
                    tasks_gradient[c].run();

                ++counter;

#pragma omp for
                for (long c = 0; c < long(tasks_move.size()); ++c)
                    tasks_move[c].run();

                if (sphere_radius) {
#pragma omp single
                    {
                        double z = 0;
                        for (const Protofilament &proto : protofilaments) {
                            assert(proto.force_sphere);
                            z += proto.force_sphere->center.z;
                        }

                        z /= number_of_proto;
                        for (Protofilament &proto : protofilaments)
                            proto.force_sphere->center.z = z;
                    }
                }

#pragma omp single
                {
                    if (!(counter % step_polymerisation)) {
                        printf("polymerisation %ld\n", counter);
                        polymerisation();
                    }
                    if (!(counter % step_hydrolysis)) {
                        printf("hydrolysis %ld\n", counter);
                        hydrolysis();
                    }
                    if (!(counter % check_overcurled_rate)) {
                        printf("check overcurled %ld\n", counter);
                        overcurled();
                    }
                    if (!(counter % update_frame_rate)) {
                        printf("update frame %ld\n", counter);
                        update_frame();
                    }
                    if (!(counter % write_snapshot_rate)) {
                        printf("write snapshot %ld\n", counter);
                        snapshot(counter);
                    }
                    if (sphere_radius && !(counter % check_sphere_rate)) {
                        if (sphere_distance_exceeded()) {
                            printf("sphere distance exceeded %ld\n", counter);
                            running = false;
                        }
                    }
                }
            }
        }
        printf("finish at %ld ns from epoch\n",
               duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count());
    }

    void shift_frame(int value)
    {
        if (value == 0)
            return;
        if (value > 0) {
            for (auto &proto : protofilaments)
                proto.add_dimers_front(value);
        } else {
            for (auto &proto : protofilaments) {
                auto &monomers = proto.monomers;
                int end_count = 2 * abs(value);
                for (auto it = monomers.begin(); it != monomers.begin() + end_count; ++it) {
                    if (it->type == DimerType::T)
                        proto.hidden_gtp_mono.insert(it->mono_number);
                }
                monomers.erase(monomers.begin(), monomers.begin() + 2 * abs(value));
            }
        }
    }

    void update_frame()
    {
        //number_to_equlibrium monomer will be shifted to fixed_start + 1.5 * min_free_length pos
        size_t number_to_equilibrium = get_number_to_equilibrium(protofilaments);

        int shift_value = get_shift_value(number_to_equilibrium);
        if (shift_value == 0)
            return;
        printf("shift value %d\n", shift_value);
        shift_frame(shift_value);
    }

    void polymerisation()
    {
        for (auto &proto : protofilaments)
            proto.polymerisation();
    }

    void hydrolysis()
    {
        for (auto &proto : protofilaments)
            proto.hydrolysis();
    }

    void overcurled()
    {
        for (auto &proto : protofilaments)
            proto.check_overcurled();
    }

    void snapshot(int64_t counter)
    {
        std::fprintf(f_snapshot, "snapshot %ld started\n", counter);
        print_new(f_snapshot, counter);
        std::fprintf(f_snapshot, "snapshot %ld finished\n\n", counter);
    }

    bool sphere_distance_exceeded() const
    {
        double max = std::numeric_limits<double>::lowest();
        for (const auto &proto : protofilaments) {
            for (const auto &mono : proto.monomers)
                max = std::max(max, mono.center.z);
        }
        return (protofilaments.front().force_sphere->center.z - max) > sphere_stop_distance;
    }

    static inline int get_shift_value(int min_inclinated)
    {
        int shift_value = 0;//shift in dimers: < 0 - tubule shifts downward, > 0 upwards
        if (min_inclinated >= first_fixed + 2 * min_free_length ||
                min_inclinated < first_fixed + min_free_length) {
            shift_value = (first_fixed + 1.5 * min_free_length) / 2 - min_inclinated / 2;
        }
        return shift_value;
    }

    static inline size_t get_number_to_equilibrium(const Protofilaments &protofilaments)
    {
        size_t number_to_equilibrium = std::numeric_limits<size_t>::max();
        for (const auto &proto : protofilaments) {
            int c = proto.get_first_inclinated();
            if (c != -1)
                number_to_equilibrium = std::min(number_to_equilibrium, size_t(c));
            else
                number_to_equilibrium = std::min(number_to_equilibrium, proto.monomers.size());
        }
        return number_to_equilibrium;
    }

    Protofilaments protofilaments;

    struct Task
    {
        enum Type {LateralL, LateralR, Longitudal, MoveDepolymerize, FillRandom, SphereInteract};
        Task(Protofilament *proto, Type type) : proto(proto), type(type) {}
        Task(const Task &t) = default;

        inline void run()
        {
            switch (type) {
            case LateralL:
                proto->fill_lateral_gradient(*proto->left);
                break;
            case LateralR:
                proto->fill_lateral_gradient(*proto->right);
                break;
            case Longitudal:
                proto->fill_longitudal_gradient();
                break;
            case MoveDepolymerize:
                proto->move_depolymerise();
                break;
            case FillRandom:
                proto->fill_random_array();
                break;
            case SphereInteract:
                proto->force_sphere->fillInteractionAddition(proto);
                break;
//            case SphereMove:
//                proto->force_sphere->move();
            default:
                break;
            }
        }
        Protofilament *proto = nullptr;
        Type type;
    };

    std::vector<Task> tasks_gradient;
    std::vector<Task> tasks_move;

    int64_t start = 0;
    int64_t finish = 0;

    std::string launch_prefix = "";

    int step_polymerisation = kinetic_time_polymerisation / dt;
    int step_hydrolysis = kinetic_time_hydrolysis / dt;

    FILE *log = nullptr;
    FILE *f_snapshot = nullptr;
};
