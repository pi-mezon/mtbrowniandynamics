#include "constants.h"

#include "regexes.h"
#include "util.h"

const std::set<std::string> Constants::affirmatives = {"True", "true", "yes"};
const std::set<std::string> Constants::negatives = {"False", "false", "no"};

void Constants::parseFile(const std::string &file_path)
{
    if (!file_path.empty()) {
        std::smatch sm;
        std::string buf = fileToBuf(file_path.c_str());
        auto begin = buf.begin();
        auto pos = std::find(begin, buf.end(), '\n');
        while (true) {
            std::string line(begin, pos);
            if (std::regex_match(line, sm, constant_regex)) {
                std::string value = sm[2];
                for (auto &ch : value) {
                    if (ch == ',')
                        ch = '.';
                }

                auto long_it = long_constants.find(sm[1]);
                if (long_it != long_constants.end())
                    long_it->second->set_value(std::stod(value));

                auto double_it = double_constants.find(sm[1]);
                if (double_it != double_constants.end())
                    double_it->second->set_value(std::stod(value));

                auto bool_it = bool_constants.find(sm[1]);
                if (bool_it != bool_constants.end()) {
                    if (affirmatives.count(sm[2])) {
                        bool_it->second->set_value(true);
                    } else if (negatives.count(sm[2])) {
                        bool_it->second->set_value(false);
                    } else {
                        fprintf(stderr, "Unknown boolean constant value '%s'\n",
                                std::string(sm[2]).c_str());
                        exit(0);
                    }
                }
            }
            if (pos == buf.end())
                break;
            begin = pos +1;
            pos = std::find(begin, buf.end(), '\n');
        }
    }

    for (auto &dependent : dependent_long_constants)
        dependent.second->fill();

    for (auto &dependent : dependent_double_constants)
        dependent.second->fill();
}

void Constants::printConstants()
{
    std::map<std::string, const ConstInterface *> all_consts;
    for (const auto &const_pair : long_constants)
        all_consts.insert(std::make_pair(const_pair.first, const_pair.second));

    for (const auto &const_pair : double_constants)
        all_consts.insert(std::make_pair(const_pair.first, const_pair.second));

    for (const auto &const_pair : bool_constants)
        all_consts.insert(std::make_pair(const_pair.first, const_pair.second));

    for (const auto &const_pair : dependent_long_constants)
        all_consts.insert(std::make_pair(const_pair.first, const_pair.second));

    for (const auto &const_pair : dependent_double_constants)
        all_consts.insert(std::make_pair(const_pair.first, const_pair.second));

    printf("Constants\n");
    for (auto c : all_consts) {
        const ConstInterface *_const = c.second;
        printf("%-32s-%16s\t%s\n", _const->name().c_str(), _const->strValue().c_str(),
               _const->state().c_str());
    }
}

Constant<bool>::Constant(const std::string &name, bool *var, Constants *constants) :
    m_name(name), value_ptr(var)
{
    if (!var) {
        printf("name %s\n", name.c_str());
        ret_error("constant: invalid variable");
    }
    constants->add_constant(this);
}
