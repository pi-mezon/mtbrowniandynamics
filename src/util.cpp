#include "util.h"

#include <algorithm>

#include <set>

#include <chrono>

char delimiter() {
#ifdef __linux__
   return '/';
#else
   return '\\';
#endif
}

void ret_error(const char *msg) {//todo: multiple argument like printf
    printf("error: %s\n", msg);
    exit(0);
}

void ret_error(const std::string &msg)
{
    printf("error: %s\n", msg.c_str());
    exit(0);
}

std::string fileToBuf(const char *name)
{
    std::string buf;
    std::fstream stream;
    stream.open(name, std::ios_base::in);
    if (stream.good()) {
        stream.seekg(0, std::ios::end);
        buf.reserve(size_t(stream.tellg()));
        stream.seekg(0, std::ios::beg);
        buf.assign(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
        stream.close();
    } else {
        printf("can't open file %s\n", name);
    }
    return buf;
}

std::string getChronoPrefix()
{
    using namespace std::chrono;

    auto time = duration_cast<nanoseconds>(system_clock::now().time_since_epoch()).count();
    std::string prefix = std::to_string(time % int64_t(1e15)) + "_results";
    if (dir_exists(prefix)) {
        printf("caution - dir exists, your data may overwrite existing\n");
        exit(0);
    } else if (!make_dir(prefix)) {
        printf("error in dir %s creation: %d\n", prefix.c_str(), errno);
        exit(0);
    }

    return prefix;
}
