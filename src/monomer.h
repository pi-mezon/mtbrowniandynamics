#pragma once

#include "constants.h"
#include "point.h"
#include "randomgenerator.h"

#include "../test/util.h"

#include <assert.h>
#include <cstdlib>
#include <math.h>

struct Monomer;

void get_right_proto_left_proto_force_points(const Monomer *first, const Monomer *second,
                                             const Point **left, const Point **right);

double calculate_energy_der(double a_coeff, double b_coeff, double dist, double r0, double r0_pow_2);

double specific_potential_longitudal_derivation(double distance);
double harmonic_potential_longitudal_derivation(double distance);

enum class DimerType {T = 'T', D = 'D', V = '-'};
enum PairType {TT = 0, DD = 1, TD = 2, END = 3};

struct NormalArray
{
    double values [3];
};

struct Monomer
{
    Monomer(double radii, double height, double z_rot_angle, double y_rot_angle,
            DimerType type = DimerType::T, int mono_number = 0, int proto_number = 0) :
        radii(radii), z_rot_angle(z_rot_angle), y_rot_angle(y_rot_angle),
        cos_z_angle(std::cos(z_rot_angle)), sin_z_angle(std::sin(z_rot_angle)),
        minus_sin_z_angle(std::sin(- z_rot_angle)), type(type),
        mono_number(mono_number), proto_number(proto_number)
    {
        update_center();
        center.z = height;
        update_force_points();
    }

    Monomer(const Monomer &other) :
        radii(other.radii), z_rot_angle(other.z_rot_angle), y_rot_angle(other.y_rot_angle),
        cos_z_angle(other.cos_z_angle), sin_z_angle(other.sin_z_angle),
        minus_sin_z_angle(other.minus_sin_z_angle), type(other.type),
        mono_number(other.mono_number), proto_number(other.proto_number)
    {
        update_center();
        center.z = other.center.z;
        update_force_points();
        clear_gradient();
    }

    Monomer() {}

    void set_z_rot_angle(double angle)
    {
        z_rot_angle = angle;
        cos_z_angle = std::cos(z_rot_angle);
        sin_z_angle = std::sin(z_rot_angle);
        minus_sin_z_angle = std::sin(- z_rot_angle);
    }

    Monomer get_post_monomer(double y_rot_addition = 0.f, DimerType type = DimerType::T) {
        Point delta = {0, 0, 2 * rad_monomer};
        delta.rotate_y(y_rot_angle + y_rot_addition / 2);

        Monomer temp = *this;
        temp.type = type;
        temp.y_rot_angle += y_rot_addition;

        temp.radii += delta.x;
        temp.update_center();
        temp.center.z += delta.z;

        temp.update_force_points();
        ++temp.mono_number;
        return temp;
    }

    Monomer get_pre_monomer() {
        Monomer temp(R_MT, down_point.z - rad_monomer, z_rot_angle, 0, DimerType::D,
                     mono_number - 1, proto_number);
        temp.update_force_points();
        return temp;
    }

    Point center;
    double radii = R_MT;

    double z_rot_angle;
    double y_rot_angle; //todo: change to y_rot angle

    double cos_z_angle;
    double sin_z_angle;
    double minus_sin_z_angle;

    double lateral_gradient_radius_l = 0;
    double lateral_gradient_radius_r = 0;
    double lateral_gradient_height_l = 0;
    double lateral_gradient_height_r = 0;
    double lateral_gradient_angle_l = 0;
    double lateral_gradient_angle_r = 0;

    double longitudal_gradient_radius = 0;
    double longitudal_gradient_heigth = 0;
    double longitudal_gradient_angle = 0;

    double force_sphere_gradient_radius = 0.f;
    double force_sphere_gradient_heigth = 0.f;

    Point left_force_point;
    Point right_force_point;
    Point up_point;
    Point down_point;
    DimerType type;

    int mono_number;
    size_t proto_number;

    static const Point unrotated_left;
    static const Point unrotated_right;

    typedef std::pair<double /*b_lat*/,double /*lat_cutoff*/> LateralCutoffPair;
    static std::vector<LateralCutoffPair> lat_cutoff;

    double (*longitudal_intradimer_energy_derivation_function)(double /*distance*/) =
            harmonic_potential_longitudal_derivation;
    double (*longitudal_interdimer_energy_derivation_function)(double /*distance*/) =
            harmonic_interdimer ? harmonic_potential_longitudal_derivation :
                                  specific_potential_longitudal_derivation;

    inline void update_center()
    {
        center.x = radii * cos_z_angle;
        center.y = radii * sin_z_angle;
    }

    inline void update_force_points() {
        const double cos_y_rot = std::cos(y_rot_angle);
        const double sin_y_rot = std::sin(y_rot_angle);

        right_force_point = unrotated_right;
        //angle sin cos
        right_force_point.rotate_y(y_rot_angle, &sin_y_rot, &cos_y_rot);
        right_force_point.rotate_z(sin_z_angle, cos_z_angle);
        right_force_point += center;

        left_force_point = unrotated_left;
        left_force_point.rotate_y(y_rot_angle, &sin_y_rot, &cos_y_rot);
        left_force_point.rotate_z(sin_z_angle, cos_z_angle);
        left_force_point += center;

        up_point = {0, 0, rad_monomer};
        up_point.rotate_y(y_rot_angle, &sin_y_rot, &cos_y_rot);
        up_point.rotate_z(sin_z_angle, cos_z_angle);
        up_point += center;

        down_point = {0,0, - rad_monomer};
        down_point.rotate_y(y_rot_angle, &sin_y_rot, &cos_y_rot);
        down_point.rotate_z(sin_z_angle, cos_z_angle);
        down_point += center;
    }

    inline void clear_gradient()
    {
        lateral_gradient_radius_l = 0;
        lateral_gradient_radius_r = 0;
        lateral_gradient_height_l = 0;
        lateral_gradient_height_r = 0;
        lateral_gradient_angle_l = 0;
        lateral_gradient_angle_r = 0;

        longitudal_gradient_radius = 0;
        longitudal_gradient_heigth = 0;
        longitudal_gradient_angle = 0;

        force_sphere_gradient_radius = 0;
        force_sphere_gradient_heigth = 0;
    }

    inline double get_radius() const {
        return std::sqrt(power2(center.x) + power2(center.y));
    }

    inline bool is_left_of(const Monomer *other) const // if we are outside
    {
        if (this->proto_number == 0 && other->proto_number == (number_of_proto - 1))
            return true;
        if (this->proto_number == (number_of_proto - 1) && other->proto_number == 0)
            return false;
        if (this->proto_number > other->proto_number)
            return true;
        assert(this->proto_number != other->proto_number);
        return false;
    }

    inline bool is_upper(const Monomer *other) const
    {
        assert(this->proto_number == other->proto_number);
        return this->mono_number > other->mono_number;
    }

    inline void fill_lateral_gradient(const Monomer &other)
    {
        const Point *left_proto_point = nullptr;
        const Point *right_proto_point = nullptr;
        get_right_proto_left_proto_force_points(this, &other, &left_proto_point, &right_proto_point);

        const double distance = left_proto_point->get_distance(*right_proto_point);

        LateralCutoffPair *p = nullptr;
        if (this->type == DimerType::T && other.type == DimerType::T)
            p = &lat_cutoff[PairType::TT];
        else if (this->type == DimerType::D && other.type == DimerType::D)
            p = &lat_cutoff[PairType::DD];
        else
            p = &lat_cutoff[PairType::TD];

        if (distance == 0) {
            printf("lateral distance == 0: this: p %zd m %d other: p %zd %d\n",
                   this->proto_number, this->mono_number, other.proto_number, other.mono_number);
            return;
        }

        if (distance > p->second)
            return;

        const double distance_to_x_der = (left_proto_point->x - right_proto_point->x) / distance;
        const double distance_to_y_der = (left_proto_point->y - right_proto_point->y) / distance;
        const double distance_to_z_der = (left_proto_point->z - right_proto_point->z) / distance;

        Point unrotated = this->is_left_of(&other) ? unrotated_right :
                                                     unrotated_left;

        const double common = unrotated.z * std::cos(y_rot_angle) - unrotated.x * std::sin(y_rot_angle);
        const double x_to_y_rot_angle_der = cos_z_angle * common;
        const double y_to_y_rot_angle_der = sin_z_angle * common;
        const double z_to_y_rot_angle_der = - unrotated.x * std::cos(y_rot_angle)
                - unrotated.z * std::sin(y_rot_angle);

        double energy_der = calculate_energy_der(a_lat, p->first, distance, r0_lat, r0_lat_pow2);
        if (!this->is_left_of(&other))
            energy_der *= -1;

        bool right = this->proto_number > other.proto_number;

        double *radius_grad = right ? &lateral_gradient_radius_r : &lateral_gradient_radius_l;
        double *height_grad = right ? &lateral_gradient_height_r : &lateral_gradient_height_l;
        double *angle_grad = right ? &lateral_gradient_angle_r : &lateral_gradient_angle_l;

        *radius_grad += energy_der *
                (distance_to_x_der * cos_z_angle + distance_to_y_der * sin_z_angle);//????

        *height_grad += energy_der * distance_to_z_der;

        *angle_grad += energy_der *
                (distance_to_x_der * x_to_y_rot_angle_der +
                 distance_to_y_der * y_to_y_rot_angle_der +
                 distance_to_z_der * z_to_y_rot_angle_der);
    }

    static void fill_longitudal_gradient_lower_upper(Monomer &lower, Monomer &upper)
    {
        assert(lower.proto_number == upper.proto_number &&
               (upper.mono_number - lower.mono_number) == 1);
        Point up = upper.down_point;
        Point low = lower.up_point;

        double distance = up.get_distance(low); // todo: case when distance == 0
        if (distance != 0) {
            const double energy_derivation = lower.mono_number % 2 ?
                        lower.longitudal_interdimer_energy_derivation_function(distance) : //this-beta interact with upper other subunit's alpha
                        lower.longitudal_intradimer_energy_derivation_function(distance);  //this-alpha interact with upper same subunit's beta

            up.rotate_z(upper.minus_sin_z_angle, upper.cos_z_angle);
            low.rotate_z(lower.minus_sin_z_angle, lower.cos_z_angle);

            const double delta_radius_to_distance = (up.x - low.x) / distance;
            const double delta_height_to_distance = (up.z - low.z) / distance;

            const double distance_to_angle_der_lower = (std::sin(lower.y_rot_angle) * delta_height_to_distance -
                                                       std::cos(lower.y_rot_angle) * delta_radius_to_distance) * rad_monomer;

            const double distance_to_angle_der_upper = (std::sin(upper.y_rot_angle) * delta_height_to_distance -
                                                       std::cos(upper.y_rot_angle) * delta_radius_to_distance) * rad_monomer;

            // stretch component
            upper.longitudal_gradient_radius += energy_derivation * delta_radius_to_distance;
            upper.longitudal_gradient_heigth += energy_derivation * delta_height_to_distance;
            upper.longitudal_gradient_angle += energy_derivation * distance_to_angle_der_upper;

            lower.longitudal_gradient_radius -= energy_derivation * delta_radius_to_distance;
            lower.longitudal_gradient_heigth -= energy_derivation * delta_height_to_distance;
            lower.longitudal_gradient_angle += energy_derivation * distance_to_angle_der_lower;
        }

        // bend component
        double *current_b_coeff = nullptr;
        double *delta_eq = nullptr;
        if (upper.type == DimerType::T) {
            current_b_coeff = &b_coeff_T;
            delta_eq = &teta0_T;
        } else if (upper.type == DimerType::D) {
            current_b_coeff = &b_coeff_D;
            delta_eq = &teta0_D;
        } else {
            assert(false);
        }

        const double angle_gradient_addition = *current_b_coeff * (upper.y_rot_angle - lower.y_rot_angle - *delta_eq);
        upper.longitudal_gradient_angle += angle_gradient_addition;
        lower.longitudal_gradient_angle -= angle_gradient_addition;
    }

    inline void move(const NormalArray &array)
    {
        const double radius_addition = - (lateral_gradient_radius_l + lateral_gradient_radius_r +
                                         longitudal_gradient_radius + force_sphere_gradient_radius) *
                gradient_transition_coefficient +
                stochastic_transition_coefficient * array.values[0];
        const double heigth_addition = - (lateral_gradient_height_l + lateral_gradient_height_r +
                                         longitudal_gradient_heigth + force_sphere_gradient_heigth) *
                gradient_transition_coefficient +
                stochastic_transition_coefficient * array.values[1];
        const double angle_addition =  - (lateral_gradient_angle_l +lateral_gradient_angle_r +
                                         longitudal_gradient_angle) * gradient_rotation_coefficient  +
                                         stochastic_rotation_coefficient * array.values[2];

        y_rot_angle += angle_addition;

        radii += radius_addition;
        update_center();
        center.z += heigth_addition;

        update_force_points();
        clear_gradient();
    }

    void print(FILE *dest) const {
        fprintf(dest, "\tproto %zd mono %d\n", proto_number, mono_number);
        fprintf(dest, "\t\ty_rot %.8f z_rot %.8f type %c\n",y_rot_angle, z_rot_angle,
                static_cast<char>(type));
        fprintf(dest, "\t\tcenter %s\n", center.print().c_str());
        fprintf(dest, "\t\tradii %.8f\n", radii);
        fprintf(dest, "\t\tright force point %s\n", right_force_point.print().c_str());
        fprintf(dest, "\t\tleft force point %s\n", left_force_point.print().c_str());
        fprintf(dest, "\t\tup force point %s\n", up_point.print().c_str());
        fprintf(dest, "\t\tdown force point %s\n", down_point.print().c_str());
    }

    void print() const {
        printf("proto %zd mono %d\n", proto_number, mono_number);
        printf("\tcenter %s\n", center.print().c_str());
        printf("\tright force point %s\n", right_force_point.print().c_str());
        printf("\tleft force point %s\n", left_force_point.print().c_str());
        printf("\tup force point %s\n", up_point.print().c_str());
        printf("\tdown force point %s\n", down_point.print().c_str());
        printf("\n");
    }

    bool equal(const Monomer &other)
    {
       if (center != other.center  || mono_number != other.mono_number)
           return false;
       if (!roundable(z_rot_angle, other.z_rot_angle) ||
               !roundable(y_rot_angle, other.y_rot_angle))
           return false;
       if (!roundable(cos_z_angle, other.cos_z_angle) ||
               !roundable(sin_z_angle, other.sin_z_angle))
           return false;

       if (proto_number != other.proto_number)
           return false;
       if (type != other.type)
           return false;
       return true;
    }
};

//right mono holds left force point, left holds right force point
inline void get_right_proto_left_proto_force_points(const Monomer *first, const Monomer *second,
                                                    const Point **left, const Point **right)
{
    if (first->is_left_of(second)) {
        *right = &second->left_force_point;
        *left = &first->right_force_point;
    } else {
        *right = &first->left_force_point;
        *left = &second->right_force_point;
    }
}

inline double calculate_energy_der(double a_coeff, double b_coeff, double dist, double r0,
                                   double r0_pow_2)
{
    return a_coeff * dist / r0_pow_2 * (2 - dist / r0) * std::exp(- dist / r0) +
            2 * b_coeff * dist / r0_pow_2 * std::exp(- power2(dist) / r0_pow_2);
}
