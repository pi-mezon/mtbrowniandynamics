#pragma once

#include <regex>

static const std::regex proto_regex("^proto (\\d+) z_angle \\S+ iteration (\\d+)$");
static const std::regex mono_regex("^\\tproto (\\d+) mono (-?\\d+)$");
static const std::regex angles_type_regex("^\\t\\ty_rot (-?\\d+.?\\d*) z_rot (-?\\d+.?\\d*) type (\\S)");
static const std::regex name_coord_regex("^\\t\\t([\\S|\\s]+) (x \\S+ y \\S+ z \\S+)$");
static const std::regex point_reqex("^x (-?\\d+.\\d*) y (-?\\d+.\\d*) z (-?\\d+.\\d*)$");
static const std::regex radii_regex("^\\t\\tradii (-?\\d+.\\d*)$");

static const std::regex constant_regex("^(\\S+) ((?:-?\\d+[.,]?\\d*(?:e-?\\d+)?)|(?:[Tt]rue)|"
                                       "(?:[Ff]alse)|(?:yes)|(?:no))$");
static const std::regex snapshot_started("^snapshot (\\d+) started$");
