#pragma once

#include <chrono>
#include <memory>
#include <random>

#include <omp.h>

template <typename T>
class Singleton
{
    static std::shared_ptr<T> self;
protected:
    Singleton() {}
public:
    static std::shared_ptr<T> instance() {
        if (!self)
            self = std::shared_ptr<T>(new T);
        return self;
    }
};

template <typename T>
std::shared_ptr<T> Singleton<T>::self = nullptr;

class SeedGenerator : public Singleton<SeedGenerator>
{
protected:
    SeedGenerator()
    {
        int time_seed = std::chrono::system_clock::now().time_since_epoch().count();
        srand(time_seed);
    }
    friend class Singleton<SeedGenerator>;
public:
    int get_seed()
    {
        return rand();
    }
};

class RandomGeneration
{
    std::normal_distribution<double> generator;
    std::mt19937 engine;

    double left = 0.f;
    double scale = 1;

public:
    RandomGeneration(int seed, double left = 0.f, double right = 1.f) : left(left) {
        generator = std::normal_distribution<double>(0.f, 1.f);
        engine = std::mt19937(seed);

        scale = (right - left) / (engine.max() - engine.min());
    }

    inline double normal_value()
    {
        return generator(engine);
    }

    inline double uniform_value()
    {
        return left + engine() * scale;
    }
};

/* Period parameters for randomiser*/
//#include <cmath>
#define N_period 624
#define M_period 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */
class Random
{
public:
    Random(unsigned long s) {
        mti=N_period+1;
        init_genrand(s);
        r1 = 0; r2 = 0; s = 0; rho = 0;
        valid = false;

        omp_init_lock(&lock);
    }

    virtual ~Random()
    {
        omp_destroy_lock(&lock);
    }
    double Normal_dist(void);
    virtual double genrand_real2();

    omp_lock_t lock;

private:
    double r1, r2, s, rho;
    bool valid;
    void init_genrand(unsigned long s);
    unsigned long mt[N_period]; /* the array for the state vector  */
    int mti;					/* mti==N_period+1 means mt[N_period] is not initialized */
};
