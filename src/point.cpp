#include "point.h"


void Point::operator-=(const Point &other)
{
//    this->operator -(other);
    *this = *this - other;
}

Point Point::operator-(const Point &other) const
{
    Point p(*this);
    p.x -= other.x;
    p.y -= other.y;
    p.z -= other.z;
    return p;
}

