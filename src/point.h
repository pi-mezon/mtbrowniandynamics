#pragma once

#include <assert.h>
#include <cmath>
#include <sstream>
#include <iomanip>

#include "regexes.h"

#define power2(x) ((x) * (x))

struct Point
{
    Point(double x, double y, double z) : x(x), y(y), z(z) {}
    Point(const std::string &str)
    {
        std::smatch sm;
        assert(std::regex_match(str, sm, point_reqex));
        x = std::stof(sm[1].str());
        y = std::stof(sm[2].str());
        z = std::stof(sm[3].str());
    }
    Point() = default;

    double x;
    double y;
    double z;

    void operator-=(const Point &other);
    Point operator-(const Point &other) const;

    inline void operator+=(const Point &other)
    {
        *this = *this + other;
    }

    inline Point operator+(const Point &other) const
    {
        Point p(*this);
        p.x += other.x;
        p.y += other.y;
        p.z += other.z;
        return p;
    }

    inline double get_distance(const Point &other) const {
        return std::sqrt(power2(x - other.x) + power2(y - other.y) + power2(z - other.z));
    }

    double operator [] (size_t i) const {
        switch (i) {
        case 1:
            return x;
        case 2:
            return y;
        case 3:
            return z;
        default:
            assert(false);
        }
    }

    void rotate_z(const double &p_sin, const double &p_cos);
    void rotate_z(double angle);
    void rotate_z(double angle, const Point &center);

    void rotate_x(double angle);
    void rotate_x(double angle, const Point &center);

    void rotate_y(double angle, const double *p_sin = nullptr, const double *p_cos = nullptr);
    void rotate_y(double angle, const Point &center);

    std::string print() const {
        std::stringstream stream;
        stream << std::fixed << std::setprecision(8) << "x " << x << " y " << y <<
                  " z " << z;
        return stream.str();
    }
};

inline void Point::rotate_z(const double &p_sin, const double &p_cos)
{
    const double _x = this->x;
    const double _y = this->y;
    x = _x * p_cos - _y * p_sin;
    y = _x * p_sin + _y * p_cos;
}

inline void Point::rotate_z(double angle)
{
    const double _x = this->x;
    const double _y = this->y;
    const double sin = std::sin(angle);
    const double cos = std::cos(angle);
    x = _x * cos - _y * sin;
    y = _x * sin + _y * cos;
}

inline void Point::rotate_z(double angle, const Point &center)
{
    *this -= center;
    this->rotate_z(angle);
    *this += center;
}

inline void Point::rotate_x(double angle)
{
    Point p = *this;
    const double _sin = std::sin(angle);
    const double _cos = std::cos(angle);
    y = p.y * _cos - p.z * _sin;
    z = p.y * _sin + p.z * _cos;
}

inline void Point::rotate_x(double angle, const Point &center)
{
    *this -= center;
    this->rotate_x(angle);
    *this += center;
}

inline void Point::rotate_y(double angle, const double *p_sin, const double *p_cos)
{
    Point p = *this;
    const double _sin = p_sin ? *p_sin : sin(angle);
    const double _cos = p_cos ? *p_cos : cos(angle);
    x = p.x * _cos + p.z * _sin;
    z = - p.x * _sin + p.z * _cos;
}

inline void Point::rotate_y(double angle, const Point &center)
{
    *this -= center;
    this->rotate_y(angle);
    *this += center;
}
