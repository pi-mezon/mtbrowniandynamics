#pragma once

class Constants;

struct ConstInterface
{
    virtual std::string state() const = 0;
    virtual std::string name() const = 0;
    virtual std::string strValue() const = 0;
    virtual bool fromConfig() const = 0;
};

template <typename T>
struct Constant : ConstInterface
{
    Constant() = default;
    Constant(const std::string &name, T *var, Constants *constants);

    void set_value(const T &value)
    {
        *value_ptr = value;
        m_config = true;
    }

    bool fromConfig() const { return m_config;}
    std::string state() const override {return m_config ? "config" : "default";}
    T value() const {return *value_ptr;}
    std::string name() const override {return m_name;}
    std::string strValue() const override {
        std::ostringstream out;
        const auto abs_value = std::abs(value());
        if (abs_value >= 1e6 || (abs_value <= 1e-6 && abs_value != 0))
            out << std::scientific;
        out << value();
        return out.str();
    }

protected:
    std::string m_name;
    T *value_ptr = nullptr;
    bool m_config = false;
};
