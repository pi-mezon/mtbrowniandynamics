#include "microtubule.h"

#include "perfrunner.h"

#include <getopt.h>

struct MTPerf : PerfRunner
{
    int singleLaunch() override
    {
        auto start = system_clock::now();
//        setbuf(stdout, NULL);

        Microtubule mt(20, 20000);
        mt.execute();

        int64_t delta = duration_cast<milliseconds>(system_clock::now() - start).count();
        printf("delta time %ld ms\n", delta);
        return delta;
    }
};

int main(int argc, char **argv)
{
    const option long_opts[] = {
        {"snapshot", required_argument, nullptr, 's'},
        {"type", required_argument, nullptr, 't'},
        {"config", required_argument, nullptr, 'c'},
        {"help", no_argument, nullptr, 'h'},
        {"extend_snapshot", no_argument, nullptr, 0}};

    bool extend_snapshot = false;
    bool help = false;

    LaunchParameters parameters;

    int indexPtr = -1;
    int opt = getopt_long(argc, argv, "s:t:c:h", long_opts, &indexPtr);
    while (opt != -1) {
        if (opt == 's')
            parameters.snapshot_path = optarg;
        else if (opt == 't')
            parameters.type = DimerType(optarg[0]);
        else if (opt == 'c')
            parameters.config_path = optarg;
        else if (opt == 'h')
            help = true;

        if (opt == 0 && !strcmp("extend_snapshot", long_opts[indexPtr].name))
            extend_snapshot = true;
        opt = getopt_long(argc, argv, "s:t:c:h", long_opts, &indexPtr);
    }

    if (help) {
        printf("./MTRestart [options]\n");
        printf("\t-c|--config\t\tpath to config with constants\n");
        printf("\t-t|--type\t[T|D]\tdimer type for initial tubule for clean start\n");
        printf("\t-s|--snapshot\t\tpath to snapshot to start with\n");
        printf("\t--extend_snapshot\t\textend in snapshot directory\n");

        exit(0);
    }

    if (extend_snapshot) {
        if (parameters.config_path.empty())
            ret_error("can't extend snapshot without snapshot path");

       int pos = parameters.snapshot_path.rfind(delimiter());
       if (pos == -1) {
           parameters.launch_prefix = "./";
       } else {
           parameters.launch_prefix = std::string(parameters.snapshot_path.begin(),
                                                  parameters.snapshot_path.begin() + pos);
       }
    } else {
        parameters.launch_prefix = getChronoPrefix();
        assert(dir_exists(parameters.launch_prefix));
    }

    if (!parameters.snapshot_path.empty() && !file_exists(parameters.snapshot_path))
        ret_error("incorrect snapshot path");

    Microtubule mt(parameters);
    mt.execute();
}
