#ifndef PERFRUNNER_H
#define PERFRUNNER_H

#include <chrono>

using namespace std::chrono;

struct PerfRunner
{
    void run()
    {
        std::vector<int> results;
        for (size_t c = 0; c < numb; ++c)
            results.push_back(singleLaunch());

        printf("\n");

        float sum = 0;
        for (size_t c = 0; c < results.size(); ++c) {
            printf("%zd\t-\t%d\n", c + 1, results[c]);
            sum += results[c];
        }

        float average = float(sum) / results.size();
        float temp = 0;
        for (int r : results)
            temp += std::pow(r - average, 2);
        float disp = std::sqrt(temp / (results.size() - 1));

        printf("\naverage %f disp %f\n", sum / results.size(), disp);
    }

    virtual int singleLaunch() = 0;

    size_t numb = 10;
};

#endif // PERFRUNNER_H
