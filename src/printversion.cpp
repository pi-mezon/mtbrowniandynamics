#include "printversion.h"

#include "versiontag.h"

#include <stdio.h>

void printVersion()
{
#ifdef VersionTag
        printf("code version %s\n", VersionTag);
#endif
}
