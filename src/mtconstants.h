#pragma once

#include "constants.h"

extern double r0_lat;
extern double r0_long;
extern double a_lat;
extern double b_lat_T;
extern double b_lat_D;
extern double a_long;
extern double b_long;
extern double k_coeff;
extern double b_coeff_D;
extern double b_coeff_T;

extern double teta0_D;
extern double teta0_T;
extern double concentration;
extern double K_on;
extern double K_hydrolysis;
extern double T;
extern double dynamic_viscosity;

extern const double K_transition_gradient;
extern const double K_transition_stochastic;
extern const double K_rotation_gradient;
extern const double K_rotation_stochastic;

extern double dt;

extern double kinetic_time_polymerisation;
extern double kinetic_time_hydrolysis;

extern double b_lat_TD;

extern double r0_long_pow2;
extern double r0_lat_pow2;

extern double lateral_cutoff_TT;
extern double lateral_cutoff_DD;
extern double lateral_cutoff_TD;
extern double lateral_cutoff_level;

extern double polymerisation_probability;
extern double hydrolysis_probability;

extern double depoly_cutoff;
extern double depoly_cutoff_level;

extern const double R_MT; //todo: rename
extern const double rad_monomer;

extern const double pi;

extern const int N_d;

extern double gradient_transition_coefficient;
extern double stochastic_transition_coefficient;
extern double gradient_rotation_coefficient;
extern double stochastic_rotation_coefficient;

extern long steps;
extern long update_frame_rate;
extern long write_snapshot_rate;
extern long check_overcurled_rate;
extern long check_sphere_rate;

extern bool harmonic_interdimer;

extern double sphere_x;
extern double sphere_radius;
extern double repulsion_coeff;
extern double force;
extern double gradient_transition_coefficient_sphere;
extern double stochastic_transition_coefficient_sphere;
extern double K_transition_constant_force;
extern double sphere_constant_force_addition;
extern double sphere_max_z;
extern double sphere_stop_distance;

///////////////////////////////////////////////////////////////////////////////////////////////
extern const double phi_right;
extern const double theta_right;

extern const double phi_left;
extern const double theta_left;

extern const size_t number_of_proto;

extern const double frame_delta;
extern const int min_free_length;
extern const int first_fixed;

double energy(double r, double a_coeff, double b_coeff, double r0);

inline double numDer(double pos, double b_lat, double r0, double delta = 0.001)
{
    return (energy(pos + delta, ::a_lat, b_lat, r0) - energy(pos - delta, ::a_lat, b_lat, r0)) /
            (2 * delta);
}

struct MtConstants : Constants
{
    static void processConstantsFile(const std::string &file_path) {
        MtConstants constants;
        constants.parseFile(file_path);

        constants.postParseActions();

        constants.printConstants();

        constants.printAdditions();
    }

protected:
    MtConstants() : Constants()
    {
        ::harmonic_interdimer = false;
    }

    void postParseActions() {
        ::b_lat_TD = (::b_lat_T + ::b_lat_D) / 2;

        ::lateral_cutoff_TT = energyLevelDistance(::a_lat, ::b_lat_T, ::r0_lat,
                                                  lateral_cutoff_level);
        ::lateral_cutoff_DD = energyLevelDistance(::a_lat, ::b_lat_D, ::r0_lat,
                                                  lateral_cutoff_level);
        ::lateral_cutoff_TD = energyLevelDistance(::a_lat, ::b_lat_TD, ::r0_lat,
                                                  lateral_cutoff_level);

        ::depoly_cutoff = energyLevelDistance(::a_long, ::b_long, ::r0_long, depoly_cutoff_level);

        ::gradient_transition_coefficient = K_transition_gradient * dt / (rad_monomer * ::dynamic_viscosity);
        ::stochastic_transition_coefficient = K_transition_stochastic * std::sqrt(::dt * ::T / (rad_monomer * ::dynamic_viscosity));
        ::gradient_rotation_coefficient = K_rotation_gradient * dt / (std::pow(rad_monomer, 3) * ::dynamic_viscosity);
        ::stochastic_rotation_coefficient = K_rotation_stochastic * std::sqrt(::T * dt / (std::pow(rad_monomer, 3) * ::dynamic_viscosity));

        ::gradient_transition_coefficient_sphere = ::K_transition_gradient * ::dt / (::sphere_radius * ::dynamic_viscosity);
        ::stochastic_transition_coefficient_sphere = ::K_transition_stochastic * std::sqrt(::dt * ::T / (::sphere_radius * ::dynamic_viscosity));

        if (::sphere_radius == 0) {
            ::sphere_constant_force_addition = 0;
        } else {
            ::sphere_constant_force_addition = ::K_transition_constant_force * ::dt * ::force /
                    (::sphere_radius * ::dynamic_viscosity); // 2.7*10^7 *force
        }
    }

    void printAdditions() {
        printf("%-32s-%16e\n", "dt", ::dt);

        printf("\n%-32s-%16.6f\n", "lat_cutoff_TT", ::lateral_cutoff_TT);
        printf("%-32s-%16.6f\n", "lat_cutoff_DD", ::lateral_cutoff_DD);
        printf("%-32s-%16.6f\n", "lat_cutoff_TD", ::lateral_cutoff_TD);

        printf("%-32s-%16.6f\n", "lat_cutoff lvl", ::lateral_cutoff_level);

        printf("\n%-32s-%16.6f\n", "depoly_cutoff", ::depoly_cutoff);

        printf("%-32s-%16.6f\n", "depoly_cutoff lvl", ::depoly_cutoff_level);
    }

private:
    //caution! specific potential kind assumed
    double energyLevelDistance(double a_coeff, double b_coeff, double r0, double cutoff_level)
    {
        const double max_r = 20; //nm
        const double step = 0.5; //nm

        if (fabs(energy(max_r, a_coeff, b_coeff, r0)) > cutoff_level)
            ret_error("energy at right bound > lateral_cutoff_level");

        double left = max_r;
        double right = max_r;
        while (left > 0 && fabs(energy(left, a_coeff, b_coeff, r0)) <= cutoff_level)
            left -= step;

        while ((right - left) > 1e-6) {
            const double pos = (left + right) / 2;
            fabs(energy(pos, a_coeff, b_coeff, r0)) > cutoff_level ? left = pos : right = pos;
        }

        return left;
    }

    Constant<double> r0_long = {"r0_long", &::r0_long, this};
    Constant<double> r0_lat = {"r0_lat", &::r0_lat, this};
    Constant<double> a_lat = {"a_lat", &::a_lat, this};
    Constant<double> b_lat_T = {"b_lat_T", &::b_lat_T, this};
    Constant<double> b_lat_D = {"b_lat_D", &::b_lat_D, this};
    Constant<double> a_long = {"a_long", &::a_long, this};
    Constant<double> b_long = {"b_long", &::b_long, this};
    Constant<double> k_coeff = {"k_coeff", &::k_coeff, this};
    Constant<double> b_coeff_D = {"b_coeff_D", &::b_coeff_D, this};
    Constant<double> b_coeff_T = {"b_coeff_T", &::b_coeff_T, this};
    Constant<double> teta0_D = {"teta0_D", &::teta0_D, this};
    Constant<double> teta0_T = {"teta0_T", &::teta0_T, this};
    Constant<double> concentration = {"concentration", &::concentration, this};
    Constant<double> K_on = {"K_on", &::K_on, this};
    Constant<double> K_hydrolysis = {"K_hydrolysis", &::K_hydrolysis, this};
    Constant<double> kinetic_time_polymerisation = {"kinetic_time_polymerisation",
                                                   &::kinetic_time_polymerisation, this};
    Constant<double> kinetic_time_hydrolysis = {"kinetic_time_hydrolysis", &::kinetic_time_hydrolysis, this};
    Constant<double> T = {"T", &::T, this};
    Constant<double> dynamic_viscosity = {"dynamic_viscosity", &::dynamic_viscosity, this};

    Constant<long> steps = {"steps", &::steps, this};
    Constant<long> update_frame_rate = {"update_frame_rate", &::update_frame_rate, this};
    Constant<long> write_snapshot_rate = {"write_snapshot_rate", &::write_snapshot_rate, this};
    Constant<long> check_overcurled_rate = {"check_overcurled_rate", &::check_overcurled_rate, this};
    Constant<long> check_sphere_rate = {"check_sphere_rate", &::check_sphere_rate, this};

    Constant<bool> harmonic_interdimer = {"harmonic_interdimer", &::harmonic_interdimer, this};

    Constant<double> sphere_x = {"sphere_x", &::sphere_x, this};
    Constant<double> sphere_radius = {"sphere_radius", &::sphere_radius, this};
    Constant<double> repulsion_coeff = {"repulsion_coeff", &::repulsion_coeff, this};
    Constant<double> force = {"force", &::force, this};
    Constant<double> sphere_max_z = {"sphere_max_z", &::sphere_max_z, this};
    Constant<double> sphere_constant_force_addition = {"sphere_constant_force_addition", &::sphere_constant_force_addition, this};
    Constant<double> sphere_stop_distance = {"sphere_stop_distance", &::sphere_stop_distance, this};

    DependentConstant<double> r0_long_pow2 = {"r0_long_pow2", "r0_long*r0_long", &::r0_long_pow2, this};
    DependentConstant<double> r0_lat_pow2 = {"r0_lat_pow2", "r0_lat*r0_lat", &::r0_lat_pow2, this};
    DependentConstant<double> polymerisation_probability = {"polymerisation_probability",
                                                           "concentration*K_on*kinetic_time_polymerisation/13",
                                                           &::polymerisation_probability, this};
    DependentConstant<double> hydrolysis_probability = {"hydrolysis_probability",
                                                       "K_hydrolysis*kinetic_time_hydrolysis",
                                                       &::hydrolysis_probability, this};
};
