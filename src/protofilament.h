#pragma once

#include "monomer.h"
#include "randomgenerator.h"

#include "forcesphere.h"

#include <omp.h>

#include <vector>
#include <list>

#include <unordered_set>
#include <memory>

struct Protofilament
{
    using RandomChunk = std::vector<NormalArray>;

    Protofilament(int number, Protofilament *right = nullptr) : right(right), number(number)
    {
        z_rot_angle = - number * 2 * pi / 13;
        seed = SeedGenerator::instance()->get_seed();
        printf("seed %d for %d\n", seed, number);
        gen.reset(new Random(seed));

        if (sphere_radius)
            force_sphere.reset(new ForceSphere);

        omp_init_lock(&random_lock);
    }

    ~Protofilament()
    {
        omp_destroy_lock(&random_lock);
    }

    void connect()
    {
        if (right)
            right->left = this;
    }

    inline void add_dimers_back(size_t count = 1, DimerType type = DimerType::T, double y_rot_angle = 0.f)
    {
        size_t mono_count = 2 * count;
        if (monomers.empty()) {
            double delta_h = 3 * 2 * rad_monomer / 13;
            monomers.emplace_back(Monomer(R_MT, delta_h * number, z_rot_angle, y_rot_angle, type,
                                          0, number));
            --mono_count;
        }
        auto back = monomers.back();
        for (size_t c = 0; c < mono_count; ++c) {
            monomers.emplace_back(back.get_post_monomer(y_rot_angle, type));
            back = monomers.back();
        }
    }

    inline void add_dimers_front(size_t count)
    {
        auto *front = &monomers.front();
        std::list<Monomer> temp;
        for (size_t c = 0; c < 2 * count; ++c) {
            temp.emplace_front(front->get_pre_monomer());
            front = &temp.front();
            auto it = hidden_gtp_mono.find(front->mono_number);
            if (it != hidden_gtp_mono.end()) {
                front->type = DimerType::T;
                hidden_gtp_mono.erase(it);
            }
        }
        monomers.insert(monomers.begin(), temp.begin(), temp.end());

        for (auto &mono : monomers)
            mono.clear_gradient(); // todo: not fill lateral for static monomers
    }

    void print_new(FILE *dest, int64_t counter) const
    {
        fprintf(dest, "proto %zd z_angle %f iteration %ld\n", number, z_rot_angle, counter);

        if (!hidden_gtp_mono.empty()) {
            std::vector<int> hidden_gtp(hidden_gtp_mono.begin(), hidden_gtp_mono.end());
            std::sort(hidden_gtp.begin(), hidden_gtp.end());

            fprintf(dest, "hidden gtp:");

            auto start = hidden_gtp.begin();
            auto it = hidden_gtp.begin() + 1;
            for (; it != hidden_gtp.end(); ++it) {
                if (*(it-1) + 1 != *it) {
                    it - 1 == start ? fprintf(dest, " %d", *start) : fprintf(dest, " %d-%d", *start, *(it - 1));
                    start = it;
                }
            }
            it - 1 == start ? fprintf(dest, " %d", *start) : fprintf(dest, " %d-%d", *start, *(it - 1));
            fprintf(dest, "\n");
        }

        for (const auto & mono : monomers)
            mono.print(dest);
    }

    inline void fill_lateral_gradient(const Protofilament &other)
    {
        int start_other = number == 12 && other.number == 0 ? 3 : 0;
        int start_this = number == 0 && other.number == 12 ? 3 : 0;

        int stop = std::min(int(monomers.size()) - start_this,
                            int(other.monomers.size()) - start_other);
        for (int c = 0; c < stop; ++c)
            monomers[c + start_this].fill_lateral_gradient(other.monomers[c + start_other]);
    }

    bool equal(const Protofilament &other)
    {
        if (monomers.size() != other.monomers.size())
            return false;
//        if (iteration != other.iteration)
//            return false;

        for (size_t c = 0; c < monomers.size(); ++c)
            if (!monomers[c].equal(other.monomers[c]))
                return false;
        return true;
    }

    bool correct_connected() const
    {
        for (size_t c = 1; c < monomers.size() - 1; ++c) {
            const auto &d = monomers[c];
            const auto &u = monomers[c + 1];
            double dist = d.up_point.get_distance(u.down_point);
            if (dist > 0.5)
                return false;
        }
        return true;
    }

    inline void fill_longitudal_gradient()
    {
        assert(first_fixed > 0);
        for (size_t c = first_fixed; c < monomers.size(); ++c) {
            auto &upper_mono = monomers[c];
            auto &lower_mono = monomers[c - 1];
            Monomer::fill_longitudal_gradient_lower_upper(lower_mono, upper_mono);
        }
    }

    inline void move_depolymerise()
    {
        move();
        depolymerisation();

        if (force_sphere)
            force_sphere->move();
    }

    void sphere_interact()
    {
        assert(force_sphere);
        force_sphere->clearGradient();
        force_sphere->fillInteractionAddition(this);
    }

    inline void move()
    {
        RandomChunk *values = &(random_values.front());

        size_t random_request = monomers.size() - first_fixed;
        if (values->size() < random_request) {
            size_t first_unfilled = values->size();

            values->reserve(random_request);

            omp_set_lock(&gen->lock);
            for (size_t c = first_unfilled; c < random_request; ++c)
                values->push_back({gen->Normal_dist(), gen->Normal_dist(), gen->Normal_dist()});
            omp_unset_lock(&gen->lock);

            printf("move: p %zd unfilled random size %zd\n", number, random_request - first_unfilled);
        }

        assert(values->size() >= random_request);

        for (size_t c = first_fixed; c < monomers.size(); ++c)
            monomers[c].move((*values)[c - first_fixed]);

        omp_set_lock(&random_lock);
        random_values.pop_front();
        omp_unset_lock(&random_lock);
    }

    inline void fill_random_array()
    {
        const int count = monomers.size() - first_fixed;
        RandomChunk values(count);

        omp_set_lock(&gen->lock);

        for (int c = 0; c < count; ++c) {
            values[c].values[0] = gen->Normal_dist();
            values[c].values[1] = gen->Normal_dist();
            values[c].values[2] = gen->Normal_dist();
        }

        omp_unset_lock(&gen->lock);

        omp_set_lock(&random_lock);
        random_values.push_back(values);
        omp_unset_lock(&random_lock);
    }

    void polymerisation()
    {
        omp_set_lock(&gen->lock);
        if (gen->genrand_real2() < polymerisation_probability) {
            assert(!(monomers.size() % 2));
            add_dimers_back(1, DimerType::T, teta0_T);
        }
        omp_unset_lock(&gen->lock);
    }

    inline void depolymerisation()
    {
        auto it = monomers.begin() + 1;
        auto finish = monomers.end() - 1;
        for (; it < finish; it += 2) {
            assert(it->mono_number % 2 && !((it + 1)->mono_number % 2));
            const Point &down = it->up_point;
            const Point &up = (it + 1)->down_point;
            if (up.get_distance(down) > depoly_cutoff) {
                printf("depolymerisation: proto %zd mono %d dist %f\n", it->proto_number,
                       it->mono_number, up.get_distance(down));
                break;
            }
        }

        if (it < finish)
            monomers.erase((it + 1), monomers.end());
    }

    void check_overcurled()
    {
        const double threshold = 2 * pi;
        for (auto it = monomers.begin() + 1; it != monomers.end(); ++it) {
            if (it->y_rot_angle > threshold) {
                printf("cut overcurled %f p %zd m %d\n", it->y_rot_angle, number, it->mono_number);
                monomers.erase(it->mono_number % 2 ? (it - 1) : it, monomers.end());
                break;
            }
        }
    }

    void hydrolysis()
    {
        assert(!(monomers.size() % 2));
        for (size_t c = 0; c < monomers.size(); c += 2) {
            assert(monomers[c].type == monomers[c + 1].type);
            if(monomers[c].type == DimerType::T && gen->genrand_real2() <= hydrolysis_probability) {
                monomers[c].type = DimerType::D;
                monomers[c + 1].type = DimerType::D;
            }
        }

        std::list<int> for_remove;
        for (int n : hidden_gtp_mono) {
            if (n % 2 && gen->genrand_real2() <= hydrolysis_probability) {
                assert(hidden_gtp_mono.count(n - 1));
                for_remove.push_back(n);
                for_remove.push_back(n - 1);
            }
        }
        for(int r : for_remove)
            hidden_gtp_mono.erase(r);
    }

    int get_first_inclinated() const
    {
        size_t c = first_fixed;
        for (; c < monomers.size(); ++c) {
            const Monomer &mono = monomers[c];
            if (mono.get_radius() - R_MT > frame_delta)
                break;
        }

        return c < monomers.size() ? c : -1;
    }

    std::vector<Monomer> monomers;

    omp_lock_t random_lock;
    std::list<RandomChunk> random_values;
    std::shared_ptr<Random> gen;

    Protofilament *left = nullptr;
    Protofilament *right = nullptr;

    size_t number;

    double z_rot_angle;

    int seed = -1;

    std::unordered_set<int> hidden_gtp_mono; // mono in dimer with GTP bound beta-tubulin

    std::shared_ptr<ForceSphere> force_sphere;
};

typedef std::vector<Protofilament> Protofilaments;
