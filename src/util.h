#pragma once

#include <fstream>
#include <set>
#include <string>
#include <vector>

#ifdef __linux__
#include <unistd.h>
#elif _WIN32
#endif

bool dir_exists(const std::string &path);
bool file_exists(const std::string &path);
bool make_dir(const std::string &path);

char delimiter();
void ret_error(const char *msg);
void ret_error(const std::string &msg);

std::string fileToBuf(const char *name);

template <typename T>
std::vector<std::string> split(const T &array_like, const char *delimiters)
{
    std::set<char> delims;
    for (const char *pc = delimiters; *pc; ++pc)
        delims.insert(*pc);

    std::vector<std::string> tokens;
    auto t_start = array_like.begin();
    auto t_end = array_like.begin();
    for (; t_end != array_like.end(); ++t_end) {
        if (delims.count(*t_end)) {
            tokens.emplace_back(t_start, t_end);
            t_start = t_end + 1;
        }
    }
    if (t_start != t_end)
        tokens.emplace_back(t_start, t_end);

    return tokens;
}

std::string getChronoPrefix();
