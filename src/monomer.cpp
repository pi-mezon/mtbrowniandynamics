#include "monomer.h"

const Point Monomer::unrotated_left = {rad_monomer * std::cos(phi_left) * std::sin(theta_left),
                                 rad_monomer * std::sin(phi_left) * std::sin(theta_left),
                                 rad_monomer * std::cos(theta_left)};

const Point Monomer::unrotated_right = {rad_monomer * std::cos(phi_right) * std::sin(theta_right),
                                  rad_monomer * std::sin(phi_right) * std::sin(theta_right),
                                  rad_monomer * std::cos(theta_right)};

std::vector<Monomer::LateralCutoffPair> Monomer::lat_cutoff;

double specific_potential_longitudal_derivation(double distance)
{
    return calculate_energy_der(a_long, b_long, distance, r0_long, r0_long_pow2);
}

double harmonic_potential_longitudal_derivation(double distance)
{
    return k_coeff * distance;
}
