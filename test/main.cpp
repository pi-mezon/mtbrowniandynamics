#include "util.h"

#include "../src/monomer.h"

#include "../src/randomgenerator.h"

#include "testframeshift.h"
#include "testdepolymerisation.h"

void test_point()
{
    Point a{1,2,3};
    Point b{2,2,2};
    a += b;
    assert(a.x == 3 && a.y == 4 && a.z == 5);
    b = b + a;
    assert(b.x == 5 && b.y == 6 && b.z == 7);
    b -= a;
    assert(b.x == 2 && b.y == 2 && b.z == 2);
    a = a - b;
    assert(a.x == 1 && a.y == 2 && a.z == 3);

    double a_to_b = a.get_distance(b);
    double b_to_a = b.get_distance(a);
    assert(roundable(a_to_b, sqrt(2)));
    assert(roundable(b_to_a, sqrt(2)));

    Point p = {1, 1, 1};
    Point z = {0, 0, 0};
    p.rotate_x(pi / 4);
    assert(roundable(p.get_distance(z), sqrt(3)));
    assert(equal_to(p, 1, 0, sqrt(2)));
    p = {1, 1, 1};
    p.rotate_y(pi / 4);
    assert(roundable(p.get_distance(z), sqrt(3)));
    assert(equal_to(p, sqrt(2), 1, 0));
    p = {1, 1, 1};
    p.rotate_z(pi / 4);
    assert(roundable(p.get_distance(z), sqrt(3)));
    assert(equal_to(p, 0, sqrt(2), 1));
}

void test_orientation()
{
    Monomer mono(0, 0, 0, 0, DimerType::T);
    FILE *f = fopen("single.txt", "w");
    mono.print(f);

    Point lfp = mono.left_force_point;
    Point rfp = mono.right_force_point;
    Point res = lfp + rfp;



    printf("tg %f\n", res.x / res.y);

    res.x *= 20;
    res.y *= 20;
    res.z *= 20;

    Point zero = {0,0,0};
    fprintf(f, "line %s %s\n", zero.print().c_str(), res.print().c_str());

    fclose(f);
}

void test_plane()
{
    {
        Point p = {0, 0, 0};
        Point v1 = {1, 0, 0};
        Point v2 = {0, 1, 0};

        assert(roundable(Plane(p, v1, v2).get_distance({4, 13, 7}), 7.f));
        assert(roundable(Plane(p, v1, v2).get_distance({5, 1, 13}), 13.f));
    }
    {
        Point p = {1, 0, 0};
        Point v1 = {-1, 0, 1};
        Point v2 = {-1, 1, 0};

        assert(roundable(Plane(p, v1, v2).get_distance({0,0,0}), 1 / sqrt(3)));
    }
}

#include "test_numeric_gradient.h"

void test_openmp()
{
    struct Analog
    {
        int id;
        double _lat;
        double _long;

        int iteration ;

        Analog *left;
        Analog *right;

        void fill_lat(const Analog &other) {
            _lat += 1;
            printf("lat self %d other %d value %f\n", id, other.id, _lat);

            if (this->iteration != other.iteration) {
                printf("iteration error\n");
            }
        }

        void fill_long() {
            printf("long self %d\n", id);
            _long += 1;
        }
    };

    std::vector<Analog> protofilaments;
    for (int c = 0; c < 3; ++c)
        protofilaments.push_back({c, double(c),double(c), 0});

    protofilaments[0].left = &protofilaments[1];
    protofilaments[0].right = &protofilaments[2];

    protofilaments[1].left = &protofilaments[2];
    protofilaments[1].right = &protofilaments[0];

    protofilaments[2].left = &protofilaments[0];
    protofilaments[2].right = &protofilaments[1];

    int counter = 0;

#pragma omp parallel private(counter)

    counter = 0;

    while (counter < 4) {
#pragma omp for
        for (int c = 0; c < (int)protofilaments.size(); ++c) {
            Analog &proto = protofilaments[c];

            printf("for cycle %d\n\n", c);

            proto.fill_lat(*proto.left);
            proto.fill_lat(*proto.right);

            proto.fill_long();
        }

        ++counter;

        printf("counter %d\n", counter);
#pragma omp for
        for (int c = 0; c < (int)protofilaments.size(); ++c) {
            Analog &proto = protofilaments[c];

            proto.iteration = counter;
        }
    }

    printf("here\n");
}

void test_polymerisation()
{
    TestTubule t(2);

    t.test_polymerisation();//todo: check when inclinated
}

void test_hidden_hydrolysis()
{

    TestTubule t(10);
    for (Protofilament &proto : t.protofilaments)
        proto.gen.reset(new TestRandom);

    std::vector<std::unordered_set<int>> gtp_mono_in_proto = {{0, 1}, //1
                                                              {2, 3}, //2
                                                              {4, 5}, //3
                                                              {6, 7}, //4
                                                              {8, 9}, //5
                                                              {10, 11}, //6
                                                              {0, 1, 4, 5}, //7
                                                              {2, 3, 6, 7}, //8
                                                              {4, 5, 8, 9}, //9
                                                              {6, 7, 10, 11}, //10
                                                              {0, 1, 4, 5, 8, 9}, //11
                                                              {2, 3, 6, 7, 10, 11}, //12
                                                              {0, 1, 2, 3, 4, 5} //13
                                                             };
    for (auto &proto : t.protofilaments) {
        for(auto &mono : proto.monomers) {
            if (gtp_mono_in_proto[proto.number].count(mono.mono_number))
                mono.type = DimerType::T;
        }
    }

    t.shift_frame(-8);
    t.hydrolysis();
    t.shift_frame(8);

    for (const auto &proto : t.protofilaments) {
        assert(proto.hidden_gtp_mono.empty());
        for (const auto &mono : proto.monomers)
            assert(mono.type == DimerType::D);
    }
}

#include "../src/regexes.h"

#include <fstream>
#include <algorithm>

void test_read_config()
{
    std::string proto_str("proto 4 z_angle 0.000000 iteration 0");
    std::smatch sm;
    assert(std::regex_match(proto_str, sm, proto_regex));

    std::string mono_str("\tproto 0 mono 0");
    assert(std::regex_match(mono_str, sm, mono_regex));

    std::string point_str("x 7.891360 y 1.932446 z -0.457880");
    assert(std::regex_match(point_str, sm, point_reqex));

    std::list<std::string> points = {"\t\tcenter x 8.128000 y 0.000000 z 0.000000",
                                     "\t\tright force point x 7.891360 y 1.932446 z -0.457880",
                                     "\t\tleft force point x 7.891673 y -1.932446 z 0.458041",
                                     "\t\tup force point x 8.128000 y 0.000000 z 2.000000",
                                     "\t\tdown force point x 8.128000 y 0.000000 z -2.000000"};
    for (const auto &str : points)
        assert(std::regex_match(str, sm, name_coord_regex) &&
               std::regex_match(sm[2].str(), point_reqex));

    Microtubule mt(8);
    for (size_t c = 0; c < mt.protofilaments.size(); ++c) {
        Protofilament &proto = mt.protofilaments[c];
        size_t k = c /4;
        auto it = proto.monomers.end() - k * 2;
        proto.monomers.erase(it, proto.monomers.end());
    }

    std::string angle_type_str("\t\ty_rot 0.000000 z_rot 0.000000 type D");
    assert(std::regex_match(angle_type_str, sm, angles_type_regex));

    const char *fname = "test_write_read.txt";
    FILE *f = fopen(fname, "w");
    mt.print_new(f);
    fclose(f);

    Microtubule rd(fname);
    assert(!rd.protofilaments.empty() && mt.equal(rd));

    Microtubule incl("../../test_data/inclinated.txt");
    assert(!incl.protofilaments.empty() && incl.correct_connected());
}

#include "lateral_distances.h"

int main()
{
//    test_point();
//    test_plane();
//    test_lateral_set();
//    test_longitudal_set();

//    test_force_point_plane_distance_set();

//    test_update_points();
//    test_lateral_orientation();
//    test_lateral_orientation();
//    test_numeric_gradient();

//    test_openmp();

//    test_frame_shift();
//    test_polymerisation();

//    test_hidden_hydrolysis();

//    test_depolymerisation();

//    test_read_config();

//    show_average_distance();

    combinate_longitudal(programGradientAtPoint);
    combinate_longitudal(programmGradientAtPointPaired);

    return 0;
}
