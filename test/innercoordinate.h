#ifndef INNERCOORDINATE_H
#define INNERCOORDINATE_H

#include "../src/monomer.h"

#include <string>

enum CoordType {Rad = 0, Y_rot = 1, Z = 2, EndCoord = 3};
static const std::vector<CoordType> types = {Rad, Z, Y_rot};
static const std::map<int, std::string> coord_names = {{Rad, "Rad"}, {Z, "H"}, {Y_rot, "Y_rot"}};

struct InnerCoord
{
    double rad;
    double y_rot;
    double z;
    double z_rot;

    InnerCoord() : rad(0), y_rot(0), z(0), z_rot(0) {}
    InnerCoord(double rad, double z_rot, double y_rot, double h) :
        rad(rad), y_rot(y_rot), z(h), z_rot(z_rot) {}

    InnerCoord(const Monomer &mono) {
        rad = mono.get_radius();
        z_rot = mono.z_rot_angle;
        y_rot = mono.y_rot_angle;
        z = mono.center.z;
    }

    InnerCoord(const InnerCoord &other) = default;
    InnerCoord &operator =(const InnerCoord &other) = default;
    void operator +=(const InnerCoord &other)
    {
        rad += other.rad;
        z_rot += other.z_rot;
        y_rot += other.y_rot;
        z += other.z;
    }
    InnerCoord operator +(const InnerCoord &other) const
    {
        InnerCoord c = *this;
        c += other;
        return c;
    }
    InnerCoord change(const double delta, CoordType type) const
    {
        InnerCoord ret = *this;
        double *ptr = reinterpret_cast<double *>(&ret);
        *(ptr + type) += delta;
        return ret;
    }

    void shift(const double delta, CoordType type)
    {
        double *p = reinterpret_cast<double*>(this);
        *(p + type) += delta;
    }

    Monomer make_mono(int number = 0, int proto = 0) const
    {
        return Monomer(rad, z, z_rot, y_rot, DimerType::T, number, proto);
    }

    std::string print() const
    {
        std::stringstream ss;
        ss << rad << "_" << z_rot << "_" << y_rot << "_" << z;
        return ss.str();
    }
};

#endif // INNERCOORDINATE_H
