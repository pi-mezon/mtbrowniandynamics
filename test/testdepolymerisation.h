#pragma once

#include "testtubule.h"

void test_depolymerisation()
{
    struct TestCase
    {
        std::vector<int> first_disruptions;// in monomers
        std::vector<int> second_disruptions;// in monomers

        size_t result_first_length;// in monomers
        size_t result_second_length;// in monomers

        size_t all_length; // in dimers
    };

    std::list<TestCase> testcases = {{{3, 5}, {4}, 4, 4, 7}, {{4}, {3, 4}, 4, 14, 7},
                                     {{6}, {}, 6, 8, 4}, {{1}, {2}, 2, 2, 4}, {{}, {}, 8, 8, 4}};
    for (const TestCase &c : testcases) {
        TestTubule t(c.all_length);
        Protofilament *proto = &t.protofilaments[0];
        for (int d : c.first_disruptions) {
            Monomer *mono = &proto->monomers[d];
            mono->center.x += 1.5;
            mono->update_force_points();
        }

        proto = &t.protofilaments[1];
        for (int d : c.second_disruptions) {
            Monomer *mono = &proto->monomers[d];
            mono->center.x += 1.5;
            mono->update_force_points();
        }

        for (auto &proto : t.protofilaments)
            proto.depolymerisation();

        for (const auto &proto : t.protofilaments) {
            if (proto.number == 0)
                assert(proto.monomers.size() == c.result_first_length);
            else if (proto.number == 1)
                assert(proto.monomers.size() == c.result_second_length);
            else
                assert(proto.monomers.size() == c.all_length * 2);
        }
    }
}
