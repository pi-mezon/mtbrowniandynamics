#pragma once

#include "../src/microtubule.h"
#include <limits>

class TestRandom : public Random
{
public:
    TestRandom() : Random(0) {}
    double genrand_real2() override {
        return 0;
    }
};

struct TestTubule : public Microtubule
{
    TestTubule(int length) : Microtubule(length) {}

    void test_update(int min_inclinated)
    {
        int shift_value = get_shift_value(min_inclinated);
        shift_frame(shift_value);

        print_to("frame_shift.txt");
    }

    void print_to(const char *name)
    {
        FILE *f = fopen(name, "w");
        print_new(f);
        fclose(f);
    }

    static void test_get_shift()
    {
        // we expects certain constant set for test cases set
        assert(first_fixed == 2 && min_free_length == 4);
        // first - min inclinated, second - expected result
        std::list<std::pair<int, int>> test_cases = {{2, 3}, {3, 3}, {4, 2}, {5, 2},
                                                     {6, 0}, {7, 0}, {8, 0}, {9, 0},
                                                     {10, -1}, {11, -1}, {12, -2}, {13, -2}};
        for (const auto &p : test_cases) {
            bool correct = TestTubule::get_shift_value(p.first) == p.second;
            if (!correct)
                printf("incl %d %s\n", p.first, correct ? "correct" : "incorrect");
            assert(correct);
        }
    }

    void correct_connected()
    {
        for (size_t c = 1; c < protofilaments.size() - 1; ++c) {
            const Protofilament &proto = protofilaments[c];
            assert(proto.left == &protofilaments[c + 1] && proto.right == &protofilaments[c - 1]);
            assert(proto.left->right == &proto && proto.right->left == &proto);
        }
        assert(protofilaments[0].right == &protofilaments[12]);
        assert(protofilaments[0].left == &protofilaments[1]);

        assert(protofilaments[12].right == &protofilaments[11]);
        assert(protofilaments[12].left == &protofilaments[0]);

        for (const Protofilament &proto : protofilaments) {
            size_t proto_numb = proto.number;
            for (size_t c = 0; c < proto.monomers.size() - 1; ++c) {
                const Monomer &mono = proto.monomers[c];
                const Monomer &mono_next = proto.monomers[c + 1];
                assert(proto_numb == mono.proto_number);
                assert(mono.mono_number + 1 == mono_next.mono_number);
            }
        }

        for (const Protofilament &proto : protofilaments) {
            for (const Monomer &mono : proto.monomers) {
                assert(mono.get_radius() - R_MT < 1e-6);
            }
        }
    }

    void test_polymerisation()
    {
        for (size_t c = 0; c < 4; ++c) {
            for (auto &proto : protofilaments) {
                assert(!(proto.monomers.size() % 2));
                proto.add_dimers_back();
            }
            correct_connected();
        }
    }
};

