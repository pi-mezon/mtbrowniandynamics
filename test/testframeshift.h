#pragma once

#include "testtubule.h"
#include <unordered_set>
#include <vector>

void test_frame_shift()
{
    TestTubule::test_get_shift();
    for (size_t c = first_fixed; c < 14; ++c) {
        TestTubule tubule((c + 2) / 2);
        tubule.test_update(c);

        tubule.correct_connected();
    }

    {
        struct TestCase
        {
            size_t first_length; // in dimers
            int first_inclinated; // in monomers
            size_t second_length;
            int second_inclinated;

            size_t expected_result;// in monomers
        };

        std::list<TestCase> cases = {{2, -1, 4, -1, 4}, {7, -1, 7, -1, 14}, {4, 5, 5, 6, 5},
                                     {7, 5, 5, 6, 5}, {7, -1, 5, 3, 3}, {7, 6, 2, -1, 4}};
        for (const auto &c : cases) {
            Protofilaments protofilaments;
            protofilaments.emplace_back(Protofilament(0));
            auto &first = protofilaments.back();
            first.add_dimers_back(c.first_length);
            if (c.first_inclinated != -1) {
                Monomer *mono = &first.monomers[c.first_inclinated];
                mono->center.x += 1.0;
            }

            protofilaments.emplace_back(Protofilament(1));
            auto &second = protofilaments.back();
            second.add_dimers_back(c.second_length);
            if (c.second_inclinated != -1) {
                Monomer *mono = &second.monomers[c.second_inclinated];
                mono->center.x += 1.0;
            }

            size_t res = TestTubule::get_number_to_equilibrium(protofilaments);
            assert(res == c.expected_result);
        }
    }

    {
        TestTubule t(10);
        std::vector<std::unordered_set<int>> gtp_mono_in_proto = {{0, 1}, //1
                                                                  {2, 3}, //2
                                                                  {4, 5}, //3
                                                                  {6, 7}, //4
                                                                  {8, 9}, //5
                                                                  {10, 11}, //6
                                                                  {0, 1, 4, 5}, //7
                                                                  {2, 3, 6, 7}, //8
                                                                  {4, 5, 8, 9}, //9
                                                                  {6, 7, 10, 11}, //10
                                                                  {0, 1, 4, 5, 8, 9}, //11
                                                                  {2, 3, 6, 7, 10, 11}, //12
                                                                  {0, 1, 2, 3, 4, 5} //13
                                                                 };
        for (auto &proto : t.protofilaments) {
            for(auto &mono : proto.monomers) {
                if (gtp_mono_in_proto[proto.number].count(mono.mono_number))
                    mono.type = DimerType::T;
            }
        }
        t.shift_frame(-8);
        t.shift_frame(8);

        for (const auto &proto : t.protofilaments) {
            for (const auto &mono : proto.monomers) {
                assert((mono.type == DimerType::T && gtp_mono_in_proto[proto.number].count(mono.mono_number)) ||
                        (mono.type == DimerType::D && !gtp_mono_in_proto[proto.number].count(mono.mono_number)));
            }
            assert(proto.hidden_gtp_mono.empty());
        }
    }
}
