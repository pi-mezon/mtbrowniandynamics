#ifndef TEST_NUMERIC_GRADIENT_H
#define TEST_NUMERIC_GRADIENT_H

#include "innercoordinate.h"

#include "../src/monomer.h"
#include "../src/protofilament.h"

#include <array>

const float scale_factor_float = 0.000345267;

inline float calculateEnergy(float a_coeff, float b_coeff, float r0, float dist)
{
    return a_coeff * std::pow(dist / r0, 2) * std::exp(- dist / r0) - b_coeff * std::exp(- std::pow(dist, 2) / power2(r0));
}

inline float lateralEnergy(float dist)
{
    return calculateEnergy(a_lat, b_lat_T, r0_lat, dist);
}

inline float longitudalEnergy(float dist)
{
    return calculateEnergy(a_long, b_long, r0_long, dist);
}

float numericDerivation(float f0, float f2, float h)
{
    return (f2 - f0) / (2 * h);
}

float takeLongitudalEnergy(const InnerCoord &upper, const InnerCoord &lower)
{
    Monomer upper_mono = upper.make_mono(); // todo: try ctor
    Monomer lower_mono = lower.make_mono();

    float dist = upper_mono.down_point.get_distance(lower_mono.up_point);

    return longitudalEnergy(dist) + b_coeff_T / 2 * std::pow(upper.y_rot - lower.y_rot - teta0_T, 2);
}

typedef std::array<float, CoordType::EndCoord> Gradient;

Gradient numericGradientAtPoint(const InnerCoord &_static, const InnerCoord &shiftable)
{
    Gradient result;

    bool is_lower_upper = _static.z < shiftable.z;

    float energy_value = is_lower_upper ? takeLongitudalEnergy(shiftable, _static) :
                                          takeLongitudalEnergy(_static, shiftable);

    float delta = energy_value * scale_factor_float;

    for (auto coord = 0; coord < EndCoord; ++coord) {
        InnerCoord plus_d = shiftable.change(delta, CoordType(coord));
        InnerCoord minus_d = shiftable.change(-delta, CoordType(coord));

        float plus_energy = is_lower_upper ? takeLongitudalEnergy(plus_d, _static) :
                                             takeLongitudalEnergy(_static, plus_d);
        float minus_energy = is_lower_upper ? takeLongitudalEnergy(minus_d, _static) :
                                        takeLongitudalEnergy(_static, minus_d);

        result[coord] = numericDerivation(minus_energy, plus_energy, delta);
    }
    return result;
}

//          upper   lower
std::pair <Gradient, Gradient> programmGradientAtPointPaired(const InnerCoord &_static, const InnerCoord &shiftable)
{
    Gradient result_lower;
    Gradient result_upper;

    Monomer lower = _static.make_mono(1);
    Monomer upper = shiftable.make_mono(2);

    Monomer::fill_longitudal_gradient_lower_upper(lower, upper);

    result_lower[Y_rot] = lower.longitudal_gradient_angle;
    result_lower[Rad] = lower.longitudal_gradient_radius;
    result_lower[Z] = lower.longitudal_gradient_heigth;

    result_upper[Y_rot] = upper.longitudal_gradient_angle;
    result_upper[Rad] = upper.longitudal_gradient_radius;
    result_upper[Z] = upper.longitudal_gradient_heigth;

    return {result_upper, result_lower};
}

//        upper     lower
std::pair<Gradient, Gradient> programGradientAtPoint(const InnerCoord &_static, const InnerCoord &shiftable)
{
    Gradient result_lower;
    Gradient result_upper;

    Monomer lower = _static.make_mono(1); // different energy mode
    Monomer upper = shiftable.make_mono(2);

    Monomer::fill_longitudal_gradient_lower_upper(lower, upper);

    result_lower[Y_rot] = lower.longitudal_gradient_angle;
    result_lower[Rad] = lower.longitudal_gradient_radius;
    result_lower[Z] = lower.longitudal_gradient_heigth;

    result_upper[Y_rot] = upper.longitudal_gradient_angle;
    result_upper[Rad] = upper.longitudal_gradient_radius;
    result_upper[Z] = upper.longitudal_gradient_heigth;

    return {result_upper, result_lower};
}

struct CompareResult
{
    bool error = false;
    float delta_value = 0;

    float value_ref = 0;
    float valie_check = 0;

    operator bool()
    {
        return error;
    }
};

CompareResult correctGradient(const Gradient &reference, const Gradient &checkable)
{
    CompareResult ret;

    for (int c = 0; c < EndCoord; ++c) {
        float ref = reference[c];
        float check = checkable[c];

        if (fabs(ref) < 0.001) {
            if (fabs(check) > 0.01) {
                ret.error = true;
                ret.delta_value = 0;
                return ret;
            }
            continue;
        }

        float delta = fabs(1 - check / ref);
        if (delta > 1e-2) {
            ret.error = true;
            ret.delta_value = std::max(delta, ret.delta_value);
            ret.valie_check = check;
            ret.value_ref = ref;
        }
    }

    return ret;
}

struct DumbBucket
{
    DumbBucket(const float low, const float high, size_t size = 10) : low(low), high(high), delta((high - low) / size)
    {
        buckets = std::vector<size_t>(size);
    }

    bool push(const float value)
    {
        if (value < low && value > high) {
            printf("value %f exceeds boundaries (%f, %f)\n", value, low, high);
            return false;
        }

        int c = (value - low) / delta;
        ++buckets[c];

        return true;
    }

    void print() const
    {
        for (size_t c = 0; c < buckets.size(); ++c)
            printf("\t[%.2f - %.2f] %zd\n", c * delta, (c + 1) * delta, buckets[c]);
        printf("\n");
    }

    size_t count() const
    {
        size_t sum = 0;
        for (auto b : buckets)
            sum += b;
        return sum;
    }

    std::vector<size_t> buckets;

    const float low;
    const float high;

    const float delta;
};

typedef std::pair <Gradient, Gradient> (*ProgrammGradientPtr)(const InnerCoord &_static, const InnerCoord &shiftable);

void combinate_longitudal(const ProgrammGradientPtr grad_func, bool verbose = false)
{
    const float delta_step = 0.02;

    int count_all = 0;
    std::list<CompareResult> error_results;

    for (size_t proto_number = 0; proto_number < number_of_proto; ++proto_number) {
        Protofilament p(proto_number);

        p.add_dimers_back();

        Monomer m1 = p.monomers[0];
        Monomer m2 = p.monomers[1];

        InnerCoord lower(m1);
        InnerCoord upper(m2);

        for (int z = 0; z < 20; ++z) { // Z shift
            InnerCoord upper_shifted = upper;

            upper_shifted.shift(delta_step * z, Z);
            for (int r = -20; r <= 20; ++r) { // R shift
                upper_shifted.shift(delta_step * r, Rad);
                for (int angle = -20; angle <= 20; ++angle) { //Angle shift
                    upper_shifted.shift(delta_step * angle, Y_rot);

                    Gradient numeric_upper = numericGradientAtPoint(lower, upper_shifted);
                    Gradient numeric_lower = numericGradientAtPoint(upper_shifted, lower);
                    auto/*upper, lower*/ program = grad_func(lower, upper_shifted);

                    if (CompareResult res = correctGradient(numeric_upper, program.first)) {
                        if (verbose) {
                            printf("Wrong gradient\n"
                                   "proto number %zd\tshifts r %d z %d angle %d upper gradient\n",
                                   proto_number, r, z, angle);
                        }
                        error_results.push_back(res);
                    }

                    if (CompareResult res = correctGradient(numeric_lower, program.second)) {
                        if (verbose) {
                            printf("Wrong gradient\n"
                                   "proto number %zd\tshifts r %d z %d angle %d lower gradient\n",
                                   proto_number, r, z, angle);
                        }
                        error_results.push_back(res);
                    }

                    ++count_all;
                }
            }
        }
    }

    DumbBucket histogram(0, 2, 10);

    size_t count_small_value_error = 0;
    for (const CompareResult &r : error_results) {
        if (r.delta_value)
            histogram.push(r.delta_value);
        else
            ++count_small_value_error;
    }

    printf("longitudal combination: all %d error(%zd): small value %zd, big delta %zd\n", count_all * 2,
           count_small_value_error + histogram.count(),
           count_small_value_error, histogram.count());
    histogram.print();
}

void combinate_lateral()
{

}

#endif // TEST_NUMERIC_GRADIENT_H

