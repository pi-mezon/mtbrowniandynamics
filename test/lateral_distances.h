#ifndef LATERAL_DISTANCES_H
#define LATERAL_DISTANCES_H

#include "../src/microtubule.h"

void show_average_distance()
{
    Microtubule mt(4);

    FILE *f = fopen("dist.txt", "w");

    mt.print_new(f);

    fclose(f);

    std::list<double> distances;

    printf("distances between lateral force points\n");

    for (const auto &proto : mt.protofilaments) {
        const Protofilament &first = proto;
        const Protofilament &second = *proto.left;

        int start_second = first.number == 12 && second.number == 0 ? 3 : 0;
        int start_first = first.number == 0 && second.number == 12 ? 3 : 0;

        size_t stop = std::min(first.monomers.size() - start_first, second.monomers.size() - start_second);
        for (size_t c = 0; c < stop; ++c) {
            const Monomer &mono_first = first.monomers[c + start_first];
            const Monomer &mono_second = second.monomers[c + start_second];
            const Point *a = nullptr;
            const Point *b = nullptr;

            get_right_proto_left_proto_force_points(&mono_first, &mono_second, &a, &b);

            double distance = a->get_distance(*b);
            printf("p %zd m %d - p %zd m %d %f\n", mono_first.proto_number, mono_first.mono_number,
                   mono_second.proto_number, mono_second.mono_number, distance);
            distances.push_back(distance);
        }
    }

    double sum = 0;
    for (auto d : distances)
        sum += d;
    sum /= distances.size();
    printf("\naverage distance %f, averaged by %zd monomers\n", sum, distances.size());
}

#endif // LATERAL_DISTANCES_H

