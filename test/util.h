#pragma once

//#include "../src/constants.h"
#include "../src/mtconstants.h"
#include "../src/point.h"

#include <sstream>

inline bool roundable(const float &value, const float &to, const float prec = 1e-7)
{
    return fabs(value - to) < prec;
}

inline bool equal_to(const Point &p, const float &x, const float &y, const float &z)
{
    return roundable(p.x, x) && roundable(p.y, y) && roundable(p.z, z);
}

inline bool equal_to(const Point &first, const Point &second)
{
    return roundable(first.x, second.x) && roundable(first.y, second.y) &&
            roundable(first.z, second.z);
}

inline bool operator ==(const Point &lhs, const Point &rhs)
{
    return roundable(lhs.x, rhs.x) && roundable(lhs.y, rhs.y) && roundable(lhs.z, rhs.z);
}

inline bool operator !=(const Point &lhs, const Point &rhs)
{
    return !(lhs == rhs);
}

struct Data
{
    int proto_numb;
    float x;
    float y;
    float theta;

    Point get_center_x() const
    {
        Point center = {R_MT + x, 0, y};
        center.rotate_z(get_z_rot_angle());
        return center;
    }

    float get_z_rot_angle() const
    {
        return pi / 2 + (1 - proto_numb) * 2 * pi / 13;
    }

    std::string print() const {
        std::stringstream ss;
        ss << "proto number " << proto_numb << " rad - R_MT " << x << " z " << y <<
              " theta " << theta;;
        return ss.str();
    }
};

struct Plane
{
    Plane(Point p, Point v, Point w) // v - first vector, w - second vector
    {
        a = v[2] * w[3] - v[3] * w[2];
        b = w[1] * v[3] - w[3] * v[1];
        c = v[1] * w[2] - v[2] * w[1];
        d -= p.x * a + p.y * b + p.z * c;
    }

    inline float get_distance(Point point) {
        return fabs(a * point.x + b * point.y + c * point.z + d) /
                sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2));
    }

private:
    // plane determines as ax + by + cz + d = 0
    float a = 0;
    float b = 0;
    float c = 0;
    float d = 0;
};
